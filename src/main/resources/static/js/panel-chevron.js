/**
 * Created by Wojciech on 28.09.2015.
 */

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('.panel.panel-default').on('hidden.bs.collapse', toggleChevron);
$('.panel.panel-default').on('shown.bs.collapse', toggleChevron);