/**
 * Created by Wojciech on 21.11.2015.
 */
angular.module('app.teacher')
    .factory('gradesService', function($http) {
        return {
            findGradesByExamId: function(id){
                return $http.get("/grades/teacher/all/"+id);
            },
            findOpenQuestionsToGrade: function(solvingExamId){
                return $http.get("/grades/teacher/openquestions/"+solvingExamId);
            },
            saveResultForOpenQuestion: function(data){
                return $http.post("/grades/teacher/result/", data);
            }

        }
    });