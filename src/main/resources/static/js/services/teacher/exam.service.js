/**
 * Created by Wojciech on 19.10.2015.
 */
angular.module('app.teacher')
    .factory('examService', function($http) {
        return {
            saveNewExam: function(data){
                return $http.post("/exam/teacher/add", data);
            },
            findMyExams: function(){
                return $http.get("/exam/teacher/myExams");
            },
            removeById: function(id){
                return $http.delete("/exam/teacher/remove/" + id);
            }
        }
    });