/**
 * Created by Wojciech on 29.10.2015.
 */
angular.module('app.student')
    .factory('solveexamService', function($http) {
        return {
            approveExamCredentials: function(examId, data){
                return $http.post('/solveExam/authorize/'+examId, data);
            },
            availableExams: function(){
                return $http.get('/')
            },
            getQuestion: function(nav, examId){
                return $http.get("/solveExam/question/"+nav+"/"+examId);
            },
            findAllQuestionIds: function(solvingExamId, examId){
                return $http.get("/solveExam/findAllAnswers/"+solvingExamId+"/"+examId);
            },
            saveQuestionAnswer: function(examId, data){
                return $http.post('/solveExam//answer/save/'+examId, data);
            },
            findAnswerById: function(solvingExamId, examId, questionId){
                return $http.get("/solveExam/findAnswer/"+solvingExamId+"/"+examId+"/"+questionId);
            },
            exitExam: function(solvingExamId, examId){
                return $http.get("/solveExam/exitExam/"+solvingExamId+"/"+examId);
            }
        }
    });