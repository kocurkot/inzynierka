/**
 * Created by Wojciech on 21.11.2015.
 */
angular.module('app.student')
    .factory('gradesService', function($http) {
        return {
            findMyGrades: function(){
                return $http.get("/grades/student/all/");
            }

        }
    });