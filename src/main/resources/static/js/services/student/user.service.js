/**
 * Created by Wojciech on 19.10.2015.
 */
angular.module('app.student')
    .factory('userService', function($http) {
        return {
            addTeacherResourcesAccess: function(data){
                return $http.post('/user/student/accessPassword', data);
            },
            getMyTeachers: function(){
                return $http.get("/user/student/myTeachers");
            },
            saveNewPassword: function(data){
                return $http.post('/user/changepassword', data);
            }
        }
    });