/**
 * Created by Wojciech on 05.11.2015.
 */
angular.module('app.student')
    .factory('examService', function($http) {
        return {
            availableExams: function(){
                return $http.get('/exam/student/availableExams');
            }
        }
    });