angular.module('app.teacher')
    .factory('userService', function($http) {
        return {
            getStudentAccessPassword: function(){
                return $http.get('/user/teacher/studentAccessPassword');
            },
            saveStudentAccessPassword: function(data){
                return $http.post('/user/teacher/saveStudentAccessPassword', data);
            },
            saveNewStudent: function(data){
                return $http.post('/user/teacher/newstudent', data);
            },
            saveNewPassword: function(data){
                return $http.post('/user/changepassword', data);
            }
        }

    });;