angular.module('app.teacher')
    .factory('testManagerService', function($http) {
        return{
            saveTestOptions: function(data){
                return $http.post('/testManager/saveTestOptions',data);
            },
            findMyTests: function(){
                return $http.get('/testManager/findMyTests');
            },
            findMyTestsForNewExam: function(){
                return $http.get('/testManager/findMyTests/newexam');
            },
            removeTestById: function(id){
                return $http.delete('/testManager/removeTest/' + id);
            }
        };

    })
    .factory('questionsService', function($resource) {
        return $resource('/testManager/saveNewQuestion',{
           save:{method:'POST'}
        });

    });
