angular.module('app.teacher', ['ngResource', 'ui.select', 'ngSanitize', 'ui.date', 'ui.router'])
    .run(function ($http) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $http.defaults.headers.common[header] = token;
    })
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("dashboard");

        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/teacher/dashboard'
            })
            .state('mytests', {
                url: '/mytests',
                templateUrl: '/testManager/show',
                controller: 'myTestsController',
                controllerAs: 'myTestsCtrl'
            })
            .state('myexams', {
                url: '/myexams',
                templateUrl: 'exam/teacher/show',
                controller: 'myExamsController',
                controllerAs: 'myExamsCtrl'
            })
            .state('newtest', {
                url: '/newtest',
                templateUrl: '/testManager/new',
                controller: 'newTestController',
                controllerAs: 'test'
            })
            .state('prepareexam', {
                url: '/prepareexam',
                templateUrl: '/exam/teacher/prepare',
                controller: 'newExamController',
                controllerAs: 'newExamCtrl'
            })
            .state('usersettings', {
                url: '/usersettings',
                templateUrl: '/user/teacher/settings/show',
                controller: 'userSettingsController',
                controllerAs: 'userCtrl'
            })
            .state('examgrades', {
                url: '/examgrades',
                templateUrl: '/grades/teacher/exam/show',
                controller: 'gradesController',
                controllerAs: 'gradesCtrl'
            })
            .state('gradeopenquestions', {
                url: '/gradeopenquestions/:solvingExamId',
                templateUrl: function ($stateParams) {
                    return '/grades/teacher/gradeopenquestions/' + $stateParams.solvingExamId;
                },
                controller: 'gradeOpenQuestionsController',
                controllerAs: 'gradesCtrl'
            })
            .state('newstudent', {
                url: '/newstudent',
                templateUrl: '/user/teacher/newstudent/show',
                controller: 'newStudentController',
                controllerAs: 'newStudentCtrl'
            })
            .state('passwordchange', {
                url: '/passwordchange/',
                templateUrl:"/user/changepassword",
                controller: 'changepasswordController',
                controllerAs: 'changepasswordCtrl'
            })

        ;
    });