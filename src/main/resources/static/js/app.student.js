/**
 * Created by Wojciech on 18.10.2015.
 */
angular.module('app.student', ['ngStomp', 'ui.router'])
    .run(function ($http) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $http.defaults.headers.common[header] = token;
    }).config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("dashboard");

    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/student/dashboard'
        })
        .state('myteachers', {
            url: '/myteachers',
            templateUrl: '/teacher/myteachers/show',
            controller: 'myTeachersController',
            controllerAs: 'myTeachersCtrl'
        })
        .state('availableexams', {
            url: '/availableexams',
            templateUrl: '/exam/student/available',
            controller: 'availableExamsController',
            controllerAs: 'availableExamsCtrl'
        })
        .state('examaccess', {
            url: '/examaccess/:examid',
            templateUrl: function ($stateParams) {
                return '/solveExam/show/' + $stateParams.examid;
            },
            controller: 'solveExamAccessController',
            controllerAs: 'accessCtrl'
        })
        .state('mygrades', {
            url: '/mygrades/',
            templateUrl:"/grades/student/mygrades/show",
            controller: 'myGradesController',
            controllerAs: 'myGradesCtrl'
        })
        .state('passwordchange', {
            url: '/passwordchange/',
            templateUrl:"/user/changepassword",
            controller: 'changepasswordController',
            controllerAs: 'changepasswordCtrl'
        })

    ;
});
