/**
 * Created by Wojciech on 23.11.2015.
 */
angular.module('app.student')
    .controller('myGradesController', function (gradesService) {
        var self = this;
        gradesService.findMyGrades().then(
            function(response){
                self.myGrades = response.data;
            },
            function (response) {
                swal("błąd pobrania ocen !", response.statusText, "error");
            }
        )
    });