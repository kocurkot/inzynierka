/**
 * Created by Wojciech on 19.10.2015.
 */
angular.module('app.student')
    .controller('myTeachersController', function (userService) {
        this.teacherResourcesAccess = {};
        var self = this;

        this.getMyTeachers = function(){
            userService.getMyTeachers().then(
                function(response){
                    self.myTeachers = response.data;
                },
                function(response){
                    self.errorGetMyTeacher = "błąd pobrania nauczycieli " + response.status + ": " + response.statusText;
                }
            );
        };

        this.getMyTeachers();
        this.addTeacherResourcesAccess = function(){
            userService.addTeacherResourcesAccess(this.teacherResourcesAccess).then(function(){
                self.getMyTeachers();
                self.teacherResourcesAccess = {};
                swal({
                    title: "nauczyciel dodany prawidłowo",
                    type: "success"
                });
            }, function(response){
                if(response.status = 401){
                    swal("niepoprawne dane !", response.statusText, "error");
                }else{
                    swal("nieoczekiwany błąd", response.statusText, "error");
                }

            })
        };



    });