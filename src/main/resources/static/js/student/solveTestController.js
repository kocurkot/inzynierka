/**
 * Created by Wojciech on 27.10.2015.
 */
/**
 * Created by Wojciech on 19.10.2015.
 */
angular.module('app.student')
    .factory('solveTestSocket', function ($stomp) {

        return $stomp;
    })

    .controller('solveTestController', function ($scope, $stomp, $location, solveexamService, $timeout, $state) {

        var self = this;

        var ACTUAL = "ACTUAL";
        var NEXT = "NEXT";
        var PREVIOUS = "PREVIOUS";

        this.openAnswer = '';
        this.keyAnswer = '';

        var shuffleArray = function (o) {
            for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        }

        var currentQuestionId;
        var setQuestion = function (question) {
            console.log(question);
            if(currentQuestionId != undefined){
                $('#'+currentQuestionId).modernBlink('stop');
            }
            self.question = question;
            currentQuestionId = question.id;

            var currentAnswer = {};
            findAllQuestionIds();

            solveexamService.findAnswerById(self.solvingExamId, examId, question.id).then(
                function(response){
                    currentAnswer = response.data;

                    if (self.question.type === "CLOSE") {
                        console.log(currentAnswer);

                        if(currentAnswer.closeAnswers){
                            for(var i = 0; i < currentAnswer.closeAnswers.length; i++){
                                var closeAnswerId = currentAnswer.closeAnswers[i].id;
                                self.closeAnswer[closeAnswerId] = currentAnswer.closeAnswers[i].isProper;
                            }
                        }
                        else{
                            self.closeAnswer = {};
                        }

                    }
                    else if(self.question.type === "OPEN"){
                        self.openAnswer = currentAnswer.answerTextOrKey;
                    }
                    else if(self.question.type === "KEY"){
                        self.keyAnswer = currentAnswer.answerTextOrKey;
                    }


                },
                function(response){
                    swal("nieoczekiwany błąd", response.statusText, "error");
                }
            );

            if (self.question.type === "CLOSE") {
                self.question.closeAnswers = shuffleArray(self.question.closeAnswers);
            }
            $timeout(function() {
                $('#'+self.question.id).modernBlink('start');

            },500);

        }

        var examId = $location.$$absUrl.split("/")[5];
        var getQuestion = function (nav) {
            solveexamService.getQuestion(nav, examId).then(
                function (response) {
                    setQuestion(response.data);
                },
                function (response) {
                    swal("nieoczekiwany błąd", response.statusText, "error");
                }
            );
        }

        var findAllQuestionIds = function () {
            solveexamService.findAllQuestionIds(self.solvingExamId, examId).then(
                function(response){
                        self.questionIds = response.data;
                },
                function(response){
                    swal("nieoczekiwany błąd", response.statusText, "error");
                }
            );
        }


        this.solvingExamId = '';

        var stomp = $stomp
            .connect('/solveTest', {}).then(function (frame) {
                console.log(frame);
                initExam();
                subscribeSolvingExamId();
            });

        this.previousQuestion = function () {
            this.question = getQuestion(PREVIOUS);
        }

        this.nextQuestion = function () {
            this.question = getQuestion(NEXT);
        }

        var subscribeClock = function (solvingExamId) {
            $stomp.subscribe('/user/topic/greetings/'+solvingExamId, function (payload, headers, res) {

                $scope.$apply(function () {
                    self.payload = payload;
                    if(self.payload <= 0){
                        swal("Koniec czasu. Test zakończony poprawnie.");
                        $state.go('availableexams');
                    }
                });
            }, {
                "headers": "are awesome"
            });
        };

        var subscribeSolvingExamId = function () {
            $stomp.subscribe('/user/topic/solveTest', function (payload, headers, res) {
                var body = JSON.parse(res.body);
                self.solvingExamId = body[0];
                self.duration = body[2];
                subscribeClock(self.solvingExamId);
                setQuestion(body[1]);
            }, {
                "headers": "are awesome"
            });
        };

        var initExam = function (callback) {
            var examId = $location.$$absUrl.split("/")[5];
            $stomp.send('/app/start/' + examId, {name: "asdf"});
            console.log("init");
        };


        this.closeAnswer = {};
        this.saveCurrentQuestion = function(){

            var data = {
                solvingExamId: this.solvingExamId,
                questionId: currentQuestionId
            };

            if(self.question.type == "OPEN"){
                data.textAnswer = this.openAnswer;
            }else if(self.question.type == "KEY"){
                data.textAnswer = this.keyAnswer;
            }else if(self.question.type == "CLOSE"){
                var closeAnswersToSend = [];
                for(var i = 0; i < self.question.closeAnswers.length; i++){
                    var answerId = self.question.closeAnswers[i].id;
                    var answerProper = this.closeAnswer[answerId];
                    if(answerProper != true){
                        answerProper = false;
                    }
                    var singleCloseAnswer = {
                        id: answerId,
                        isProper: answerProper
                    }
                    closeAnswersToSend.push(singleCloseAnswer);
                    //console.log(singleCloseAnswer);
                }

               // console.log(this.closeAnswer);
                console.log(closeAnswersToSend);
                data.closeAnswers = closeAnswersToSend;
            }

           // console.log(self.question);

            solveexamService.saveQuestionAnswer(examId, data).then(function(response){
               // console.log(response);
                    findAllQuestionIds();
            },
            function(response){
                swal("błąd zapisu odpowiedzi", response.statusText, "error");
            });
        }

        this.exitTest = function(){
            swal({
                    title: "Jesteś pewny?",
                    text: "Test zostanie zakończony bez możliwości powrotu!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "Anuluj",
                    confirmButtonText: "Zakończ!",
                    closeOnConfirm: false},
                function(){
                    solveexamService.exitExam(self.solvingExamId,examId).then(
                        function(response){
                            swal("test zakończony poprawnie.");
                            $location.replace();
                            $state.go('availableexams');
                        },
                        function(response){
                            swal("błąd zakończenia zalicznia...", response.statusText, "error");
                        }
                    );
                });
        }

    });