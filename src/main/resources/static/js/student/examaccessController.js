/**
 * Created by Wojciech on 29.10.2015.
 */
this.examAccess = {};
angular.module('app.student')
    .controller('solveExamAccessController', function(solveexamService,$window, $location){

        this.approveExamCredentials = function(){
            var examId = $location.$$absUrl.split("/")[5];
            solveexamService.approveExamCredentials(examId,this.examAccess.password).then(
                function(){
                    swal({
                        title: 'Dostęp do zaliczenia został przyznany poprawnie !',
                        type: 'success',
                        confirmButtonText: "Przejdź do rozwiązywania testu",
                        closeOnConfirm: false,
                        allowEscapeKey: false
                    }, function(){
                        $window.location.reload()
                    });
                },
                function(response){
                    if(response.status = 401){
                        swal("niepoprawne hasło !", response.statusText, "error");
                    }else{
                        swal("nieoczekiwany błąd", response.statusText, "error");
                    }

                }
            )
        };

    })
