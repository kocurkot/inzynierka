/**
 * Created by Wojciech on 19.10.2015.
 */
angular.module('app.student')
    .controller('availableExamsController', function (examService) {

        var self = this;

        examService.availableExams().then(
            function(response){
                console.log(response);
                self.availableExams = response.data;
            },
            function(response){
                swal("nieoczekiwany błąd", response.statusText, "error");
            }
        );

    });