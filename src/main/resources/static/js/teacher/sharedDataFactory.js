/**
 * Created by Wojciech on 07.10.2015.
 */
angular.module('app.teacher')
.factory('TeacherSharedData', function(){
       var data = [];

        return{
            getById: function(id){
                return data[id];
            },
            setForId: function(id, dataToSet){
                data[id] = dataToSet;
            }
        }

    });