angular.module('app.teacher')
    .controller('newStudentController', function (userService, $scope) {
        var self = this;



            this.newStudent = {};
        this.saveNewStudent = function(){
            console.log(this.newStudent);
            userService.saveNewStudent(this.newStudent).then(
                function(response){
                    console.log("a "+response);
                    swal({
                        title: 'Student zapisany poprawnie !',
                        type: 'success',
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function(){
                        $scope.$apply(function(){
                            self.newStudent = {};
                        });
                    });
                },
                function(response){
                    console.log("b "+response);
                    if(response.status == 409){
                        swal("Student o podanym adresie e-mail lub numerze albumu już istnieje w bazie danych !", "", "warning");
                    }
                    else
                    {
                        swal("nieoczekiwany błąd !", response.statusText, "error");
                    }
                }
            );
        }

    });