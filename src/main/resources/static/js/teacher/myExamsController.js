/**
 * Created by Wojciech on 08.11.2015.
 */
angular.module('app.teacher')
    .controller('myExamsController', function (examService) {

            var self = this;
            findMyExams();

            function findMyExams(){
                examService.findMyExams().then(
                    function(response){
                        self.myExams = response.data;
                    },
                    function(response){
                        swal("nieoczekiwany błąd !", response.statusText, "error");
                    }
                );
            }

        this.removeExamById = function(id){
            console.log(id);
            examService.removeById(id).then(
                function(response){
                    findMyExams();
                    swal("usunięto zaliczenie !", id, "success");

                },
                function(response){
                    swal("nieoczekiwany błąd !", response.statusText, "error");
                }
            );
        }

    }
    );