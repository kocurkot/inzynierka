/**
 * Created by Wojciech on 20.10.2015.
 */
angular.module('app.teacher')
    .controller("myTestsController", function(testManagerService){
        var self = this;
        findMyTests();

        function findMyTests(){
            testManagerService.findMyTests().then(
                function(response){
                    self.myTests = response.data;
                },
                function(response){
                    swal("nieoczekiwany błąd !", response.statusText, "error");
                }
            );
        }


        this.removeTestById = function(id){

            swal({
                    title: "Jesteś pewny?",
                    text: "Twój test zostanie usunięty!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "Anuluj",
                    confirmButtonText: "Tak, usuń!",
                    closeOnConfirm: false},
                function(){
                    testManagerService.removeTestById(id).then(
                        function(){
                            findMyTests();
                            swal('Test został usunięty','','success');
                        },
                        function(response){
                            swal("nieoczekiwany błąd !", response.statusText, "error");
                        }
                    );
                });

        };
    });