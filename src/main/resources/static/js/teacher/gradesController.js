/**
 * Created by Wojciech on 21.11.2015.
 */
angular.module('app.teacher')
    .controller('gradesController', function (gradesService, examService) {

        var self = this;

        this.keyValueExams = {};

        examService.findMyExams().then(
            function(response){
                self.myExams = response.data;

                for(var i = 0; i < self.myExams.length; i++){
                    self.keyValueExams[self.myExams[i].examDTO.id] = self.myExams[i].examDTO.name + " -> " + self.myExams[i].testDTO.name;
                }
            },
            function(response){
                swal("nieoczekiwany błąd !", response.statusText, "error");
            }
        );

        this.onChangeExamSelect = function(){

            gradesService.findGradesByExamId(this.currentExamSelect).then(
                function(response){
                    self.grades = response.data;
                },
                function(response){
                    swal("nieoczekiwany błąd !", response.statusText, "error");
                }
            )

        }

    })

    .controller('gradeOpenQuestionsController', function (gradesService, $stateParams) {
        var self = this;

        gradesService.findOpenQuestionsToGrade($stateParams.solvingExamId).then(
            function(response){
                self.questionsToGrade = response.data;
            },
            function(response){
                swal("błąd pobrania pytań do oceny !", response.statusText, "error");
            }
        );

        this.saveQuestionPoints = function(questionId, gradeId, gradePoints){

            var data = {
                questionId: questionId,
                gradeId: gradeId,
                gradePoints: gradePoints
            }

            gradesService.saveResultForOpenQuestion(data).then(
                function(result){
                },
                function(result){
                    swal("błąd zapisu !", response.statusText, "error");

                }
            );
        }

    });