angular.module('app.teacher')
    .filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function(item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        }
    })

    .controller('newExamController', function ($scope, testManagerService, examService, $state) {

        this.newExam = {};
        this.startDate = moment().format("MM/DD/YYYY HH:mm");
        this.endDate = moment().add(2,'hour').format("MM/DD/YYYY HH:mm");
        this.myTests = [];
        $('#start-datetimepicker').datetimepicker({
            format : 'MM/DD/YYYY HH:mm'
        });
        $('#end-datetimepicker').datetimepicker({
            format : 'MM/DD/YYYY HH:mm'
        });
        var self = this;

        testManagerService.findMyTestsForNewExam().success(function(data){
            self.myTests = data;
        });

        this.generatePassword = function(){
            var length = 8,
                charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
            this.newExam.password = retVal;
        }

        this.newExamFormValid = function(){
            var dateValid = this.dateValid = new Date($('#start-datetimepicker').val()) < new Date($('#end-datetimepicker').val());
            return $scope.newExamForm.$valid && this.test && this.test.selected && dateValid;
        }

        this.initPoints = function(){
            self.newExam.questionGroupedCount = {};
            //for(var i = 0; i < this.myTests.length; i++){
            //    if(this.myTests[i].id == this.test.selected){
            //        angular.forEach(this.myTests[i].questionGrupedCount, function(val,key){
            //        //    self.newExam.questionsAmount[key]= {0:0};
            //        });
            //    }
            //}
        }

        this.saveNewExam = function(){
            console.log(this.newExam.questionGroupedCount);


            this.newExam.startDate = new Date($('#start-datetimepicker').val());
            this.newExam.endDate = new Date($('#end-datetimepicker').val());
            this.newExam.testId = this.test.selected;
            this.newExam.questionsAmount = 10;
            console.log(this.newExam);
            examService.saveNewExam(this.newExam).then(
                function(){
                    swal({
                        title: 'Zaliczenie zostało zapisane poprawnie !',
                        type: 'success',
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function(){
                        //$window.location.href = '/exam/teacher/show';
                        $state.go('myexams');
                    });
                },
                function(response){
                    if(response.status == 400){
                        swal("niepoprawne dane !", response.statusText, "warning");
                    }else{
                        swal("nieoczekiwany błąd !", response.statusText, "error");

                    }
                }
            );
        }




    });