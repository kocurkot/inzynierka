angular.module('app.teacher')
    .controller('userSettingsController', function (userService, utilService) {

        var self = this;

        var users = [];

        this.generatePassword = function(){
            this.passwordForStudents = utilService.generatePassword();
        }

        userService.getStudentAccessPassword().success(function(data){
            self.passwordForStudents = data;
        }).error(function(){
            swal("Błąd pobrania hasła !")
        });

        this.saveStudentAccessPassword = function(){
            userService.saveStudentAccessPassword(this.passwordForStudents).success(function(){
                swal("Hasło zapisano poprawnie!")
            }).error(function(){
                swal("Błąd zapisu!")
            });
        }

    })