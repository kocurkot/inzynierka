angular.module('app.teacher')
    .controller('changepasswordController', function (userService) {
        var self = this;

        this.saveNewPassword = function(){
            userService.saveNewPassword(self.passwordForm).then(
                function(response){
                    swal({
                        title: 'Hasło zmieniono poprawnie !',
                        type: 'success',
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function(){
                        $state.go('dashboard');
                    });
                },
                function(response){
                    if(response.status == 401){
                        swal("nieprawidłowe dane !", response.statusText, "warning");
                    }else{
                        swal("nieoczekiwany błąd !", response.statusText, "error");

                    }
                }
            );
        }
    });

