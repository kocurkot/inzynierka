/**
 * Created by Wojciech on 04.10.2015.
 */
angular.module('app.teacher')
    .controller('newTestController', function (testManagerService, TeacherSharedData, questionsService, $state) {
        this.new = {};
        $('#collapseOptionsBody').collapse('show');
        this.newTestId = TeacherSharedData.newTestId;
        var self = this;
        this.save = function () {
            console.log(this.new);
            testManagerService.saveTestOptions(this.new).then(function (c) {
                TeacherSharedData.setForId("newTestId", c.data);
                $('#collapseOptionsBody').collapse('hide');
                self.new.id = c.data;
                alertify.success("Test zapisano pomyślnie -> teraz dodaj pytania");
            }, function () {
                alertify.error("Błąd zapisu testu");
            });
        };

        this.question = {};
        this.questions = [];
        this.question.type = "OPEN";
        this.question.closeAnswers = [];
        this.sharedData = TeacherSharedData;
        this.question.points = 2;
        this.isEmptyTags = true;


        function determineWhichTypeOfAnswersWillSave() {
            if (self.question.type === "OPEN") {
                self.question.closeAnswers = null;
                self.question.keyAnswers = null;
            } else if (self.question.type === "CLOSE") {
                self.question.keyAnswers = null;
            } else if (self.question.type === "KEY") {
                self.question.closeAnswers = null;
                self.question.keyAnswers = $(".tm-input").tagsManager('tags')
            }
        }

        this.saveNewQuestion = function () {
            determineWhichTypeOfAnswersWillSave();
            self.question.testId = self.sharedData.getById('newTestId');
            questionsService.save(self.question, function () {
                self.questions.push(self.question);
                var lastQuestionType = self.question.type;
                $(".tm-input").tagsManager('empty');
                self.isEmptyTags = isEmptyTags();
                self.question = {};
                self.question.points = 2;
                self.question.closeAnswers = [];
                self.question.type = lastQuestionType;
                alertify.success("Pytanie zapisano pomyślnie");
            }, function () {
                alertify.error("Błąd zapisu pytania");
            })
        };

        jQuery(".tm-input").on('tm:afterPush', function(e, tag) {
            alert(tag + " was pushed!");
        });

        this.isSetQuestionType = function (questionType) {
            return this.question.type === questionType;
        };

        this.addNewCloseAnswer = function () {
            this.question.closeAnswers.push({text: "", isProper: false});
        };

        this.removeAnswer = function (answer) {
            var index = self.question.closeAnswers.indexOf(answer);
            self.question.closeAnswers.splice(index, 1);
        }

        var isEmptyTags = function(){
            var tags = $(".tm-input").tagsManager('tags');
            if(tags == undefined || tags.length == 0){
                return true;
            }
            return false;
        }

        this.addNewKeyAnswer = function () {
            $(".tm-input").tagsManager('pushTag', this.keyAnswer);
            this.isEmptyTags = isEmptyTags();
        };

        this.exit = function(){
            self.question = {};
            TeacherSharedData.setForId('newTestId',undefined);
            $state.go('mytests');
        }
    });
