angular.module('app', ['ngResource', 'ui.router'], function($locationProvider){
       // $locationProvider.html5Mode(true);
    })
    .run(function ($http) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $http.defaults.headers.common[header] = token;
    })
