package examsystem;

import examsystem.services.SolvingExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.*;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.servlet.http.HttpSession;

/**
 * Created by Wojciech on 27.10.2015.
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    private SolvingExamService solvingExamService;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/solveTest").setAllowedOrigins("*").addInterceptors(new SolvingTestHttpSessionHandshakeInterceptor()).withSockJS();
    }

    private class SolvingTestHttpSessionHandshakeInterceptor extends HttpSessionHandshakeInterceptor {
        @Override
        public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception ex) {

            solvingExamService.onConnectToSolvingTest();
            super.afterHandshake(request, response, wsHandler, ex);
        }
    }
}
