package examsystem.controllers;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by Wojciech on 25.09.2015.
 */
@Controller
@RequestMapping
public class HomeController {

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String index(Principal user) {

        SimpleGrantedAuthority student = new SimpleGrantedAuthority("ROLE_STUDENT");
        SimpleGrantedAuthority teacher = new SimpleGrantedAuthority("ROLE_TEACHER");

        if(((UsernamePasswordAuthenticationToken) user).isAuthenticated()){
            if(((UsernamePasswordAuthenticationToken) user).getAuthorities().contains(teacher)){
                return "teacher/homeLayout";
            }
            else if(((UsernamePasswordAuthenticationToken) user).getAuthorities().contains(student)){
                return "student/homeLayout";
            }
            return "login";
        }
        else{
            //TODO: any page with info about not enough accessibility here
            return "login";
        }


    }

    @RequestMapping(value="student/dashboard", method = RequestMethod.GET)
    public String studentDashboard(Principal user) {
        return "student/dashboard";
    }

    @RequestMapping(value="teacher/dashboard", method = RequestMethod.GET)
    public String teacherDashboard(Principal user) {
        return "teacher/dashboard";
    }
}
