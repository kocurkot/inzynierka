package examsystem.controllers;

import examsystem.dtos.*;
import examsystem.services.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Wojciech on 21.11.2015.
 */
@Controller
@RequestMapping("/grades")
public class GradesController {

    @Autowired
    private GradeService gradeService;

    @RequestMapping(value="/teacher/exam/show", method = RequestMethod.GET)
    public String index() {
        return "teacher/examgrades::content";
    }

    @RequestMapping(value="/teacher/gradeopenquestions/{solvingExamId}", method = RequestMethod.GET)
    public String gradeopenquestions(@PathVariable String solvingExamId) {
        return "teacher/gradeopenquestions::content";
    }

    @RequestMapping(value="/student/mygrades/show", method = RequestMethod.GET)
    public String myGradesShow() {
        return "student/mygrades::content";
    }

    @RequestMapping(value="/teacher/all/{examId}", method = RequestMethod.GET)
    @ResponseBody
    public List<GradeForExamTableDTO> findGradesByExamId(@PathVariable String examId){
        List<GradeForExamTableDTO> grades = gradeService.findGradesByExamId(examId);
        return grades;
    }

    @RequestMapping(value="/teacher/openquestions/{solvingExamId}", method = RequestMethod.GET)
    @ResponseBody
    public List<QuestionsToGradeDTO> findOpenQuestionsToGrade(@PathVariable String solvingExamId){
        List<QuestionsToGradeDTO>  questions = gradeService.findOpenQuestionsToGrade(solvingExamId);
        return questions;
    }

    @RequestMapping(value="/teacher/result", method = RequestMethod.POST)
    @ResponseBody
    public void saveResultForOpenQuestion(@RequestBody OpenQuestionGradeDTO openQuestionGradeDTO){
        gradeService.saveResultForOpenQuestion(openQuestionGradeDTO);
    }

    @RequestMapping(value="/student/all", method = RequestMethod.GET)
    @ResponseBody
    public List<MyGradesDTO> findMyGrades(){
        List<MyGradesDTO> grades = gradeService.findMyGrades();
        return grades;
    }
}
