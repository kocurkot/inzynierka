package examsystem.controllers;

import examsystem.dtos.ExamDTO;
import examsystem.dtos.ExamTestDTO;
import examsystem.services.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Controller
@RequestMapping("/exam")
public class ExamController {


    @Autowired
    private ExamService examService;

    @RequestMapping(value="/teacher/prepare", method = RequestMethod.GET)
    public String index() {
        return "teacher/prepareexam::content";
    }

    @RequestMapping(value="/teacher/show", method = RequestMethod.GET)
    public String show() {
        return "teacher/myexams::content";
    }

    @RequestMapping(value="/teacher/add", method = RequestMethod.POST)
    @ResponseBody
    public void saveNewExam(@RequestBody @Valid ExamDTO examDTO) {
        examService.saveNewExam(examDTO);
    }

    @RequestMapping(value="/teacher/myExams", method = RequestMethod.GET)
    @ResponseBody
    public List<ExamTestDTO> myExams() {

        List<ExamTestDTO> myExams = examService.findMyExams();
        return myExams;
    }

    @RequestMapping(value="/student/available", method = RequestMethod.GET)
    public String availableExams() {
        return "/student/availableexams::content";
    }

    @RequestMapping(value="/student/availableExams", method = RequestMethod.GET)
    @ResponseBody
    public List<ExamTestDTO> getAvailableExams(){
        List<ExamTestDTO> availableExams = examService.getAvailableExams();
        availableExams.forEach(p-> p.getExamDTO().setPassword(null));
        availableExams.forEach(p-> p.getExamDTO().setTestId(null));
        return availableExams;
    }

    @RequestMapping(value="/teacher/remove/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> findMyTests(@PathVariable String id) {
       Boolean success = examService.removeExamById(id);
        if(success){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
