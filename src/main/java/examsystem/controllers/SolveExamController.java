package examsystem.controllers;

import examsystem.dtos.QuestionToSolveDTO;
import examsystem.dtos.UserAnswerDTO;
import examsystem.dtos.UserAnswerForSolvingEntity;
import examsystem.entities.ExamEntity;
import examsystem.services.ExamService;
import examsystem.services.QuestionNumberType;
import examsystem.services.SolvingExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Wojciech on 27.10.2015.
 */
@Controller
@RequestMapping("/solveExam")
public class SolveExamController {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private SolvingExamService examSolvingService;

    @Autowired
    private TaskScheduler taskScheduler;

    private final String examAuthenticated = "examAuthenticated";

    @RequestMapping(value="/show/{id}", method = RequestMethod.GET)
    public String index(HttpServletRequest request, @PathVariable String id) {

        if(request.getSession().getAttribute(examAuthenticated+id) != Boolean.TRUE && !examSolvingService.isExamFinished(id)){
            return "student/solveexamaccess::content";
        }

        if(!examSolvingService.isExamAvailableToSolve(id) || examSolvingService.isExamFinished(id)){
            logger.info("no acces to exam id: " + id);
            return "student/dashboard::content";
        }

        return "student/solveexam::content";
    }

    @RequestMapping(value="/authorize/{examId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> saveNewExam(HttpServletRequest request, @RequestBody String password, @PathVariable String examId) {

        if(examSolvingService.isExamAuthorized(examId, password)){
            request.getSession().setAttribute(examAuthenticated+examId,true);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    //http://localhost:8080/solveExam/question/NEXT/564381e7ab3b06207028c948
    @RequestMapping(value="/question/{questionNumberType}/{examId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<QuestionToSolveDTO> getNextQuestion(HttpServletRequest request, @PathVariable String examId,
                                                       @PathVariable QuestionNumberType questionNumberType, Principal principal) {
        if (isExamNotAuthenticated(request, examId, principal)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Integer incrementNumber = 0;

        switch(questionNumberType){
            case NEXT:
                incrementNumber++;
                break;
            case PREVIOUS:
                incrementNumber--;
                break;
            case ACTUAL:
                break;
        }


        QuestionToSolveDTO nextQuestion = examSolvingService.getQuestionByIncrementing(examId, incrementNumber);
        return new ResponseEntity<>(nextQuestion,HttpStatus.OK);
    }

    private boolean isExamNotAuthenticated(HttpServletRequest request, String examId, Principal principal) {
        Object attribute = request.getSession().getAttribute(examAuthenticated + examId);

        if(attribute == null || !attribute.equals(true)){
            logger.info("Unauthorized access to examId: " + examId + " by user: " + principal.getName());
            return true;
        }
        return false;
    }

    @RequestMapping(value="/answer/save/{examId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> saveAnswer(HttpServletRequest request, @RequestBody UserAnswerDTO userAnswerDTO, @PathVariable String examId, Principal principal) {

        if (isExamNotAuthenticated(request, examId, principal)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        examSolvingService.saveAnswer(userAnswerDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="/findAllAnswers/{solvingExamId}/{examId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map> findAllAnswerIds(HttpServletRequest request, @PathVariable String solvingExamId, @PathVariable String examId, Principal principal) {

        if (isExamNotAuthenticated(request, examId, principal)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        Map<String,Boolean> answerIds = examSolvingService.findAllAnswerIdsBySolvingExamId(solvingExamId);
        return new ResponseEntity<>(answerIds,HttpStatus.OK);
    }

    @RequestMapping(value="/findAnswer/{solvingExamId}/{examId}/{questionId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<UserAnswerForSolvingEntity> findAnswer(HttpServletRequest request, @PathVariable String solvingExamId, @PathVariable String examId, @PathVariable String questionId, Principal principal) {

//        UserAnswerForSolvingEntity userAnswerForSolvingEntity = new UserAnswerForSolvingEntity();

        if (isExamNotAuthenticated(request, examId, principal)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        UserAnswerForSolvingEntity userAnswerForSolvingEntity = examSolvingService.findAnswerBySolvingExamIdAndQuestionId(solvingExamId, questionId);

        return new ResponseEntity<>(userAnswerForSolvingEntity,HttpStatus.OK);
    }

    @RequestMapping(value="/exitExam/{solvingExamId}/{examId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> exitExam(HttpServletRequest request, @PathVariable String solvingExamId, @PathVariable String examId, Principal principal) {

        if (isExamNotAuthenticated(request, examId, principal)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        request.getSession().setAttribute(examAuthenticated+examId,null);
        examSolvingService.exitExam(solvingExamId);
//


        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    ExamService examService;

    @MessageMapping("/start/{examId}")
    @SendToUser("/topic/solveTest")
    public List<Object> start(@DestinationVariable String examId){
        System.out.println("WebSocket init... examId => " + examId);
        String solvingExamId = examSolvingService.initSolvingData(examId);
        ExamEntity examEntity = examService.getExamById(examId);
        //we sending first question by socket because need do it after init data about solving exam
        return Arrays.asList(solvingExamId,examSolvingService.getQuestionByIncrementing(examId, 0), examEntity.getDurationSeconds());
    }

}
