package examsystem.controllers;

import examsystem.dtos.TestDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

/**
 * Created by Wojciech on 26.09.2015.
 */
@Controller
@RequestMapping("teacher/myteachers")
public class TeacherController {

    @RequestMapping(value="show", method = RequestMethod.GET)
    public String index() {
        return "student/myteachers::content";
    }

}
