package examsystem.controllers;

import examsystem.dtos.ChangePasswordDto;
import examsystem.dtos.TeacherResourcesAccessDTO;
import examsystem.dtos.UserDTO;
import examsystem.exceptions.BadTeacherResourcesAccessData;
import examsystem.exceptions.BadUserRoleException;
import examsystem.exceptions.UserAlbumNumberExistsException;
import examsystem.exceptions.UserMailExistsException;
import examsystem.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value="changepassword", method = RequestMethod.GET)
    public String changepasswordView() {
        return "changepassword::content";
    }

    @RequestMapping(value="teacher/settings/show", method = RequestMethod.GET)
    public String teacherSettingsView() {
        return "teacher/usersettings::content";
    }

    @RequestMapping(value="teacher/newstudent/show", method = RequestMethod.GET)
    public String addNewStudentView() {
        return "teacher/newstudent::content";
    }



    @RequestMapping(value="teacher/studentAccessPassword", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody public String studentAccessPassword() {
        return userService.findStudentAccessPassword();
    }

    @RequestMapping(value="teacher/saveStudentAccessPassword", method = RequestMethod.POST)
    @ResponseBody public ResponseEntity<String> saveStudentAccessPassword(@RequestBody String studentAccessPassword) {
       if(studentAccessPassword == null || studentAccessPassword.length() < 3 || studentAccessPassword.length() > 15){
           return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
       }
        try {
            userService.saveStudentAccessPassword(studentAccessPassword);
        } catch (BadUserRoleException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="student/accessPassword", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> setAccessToTeacherResources(@RequestBody @Valid TeacherResourcesAccessDTO teacherResourcesAccessDTO) {

        try {
            userService.saveTeacherResourcesAccessForStudent(teacherResourcesAccessDTO);
        } catch (BadTeacherResourcesAccessData badTeacherResourcesAccessData) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="student/myTeachers", method = RequestMethod.GET)
    @ResponseBody public List<UserDTO> findMyTeachers() {
        return userService.findMyTeachers();
    }

    @RequestMapping(value="teacher/newstudent", method = RequestMethod.POST)
    public ResponseEntity<String> saveNewStudent(@RequestBody @Valid UserDTO userDTO) {
        try {
            userService.saveNewStudent(userDTO);
        } catch (UserMailExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (UserAlbumNumberExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="changepassword", method = RequestMethod.POST)
    public ResponseEntity<String> saveNewStudent(@RequestBody @Valid ChangePasswordDto userDTO) {

        if(!userDTO.getNewPassword1().equals(userDTO.getNewPassword2())
                || !userService.isPasswordGood(userDTO.getOldPassword())){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        userService.saveNewPassword(userDTO.getNewPassword1());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
