package examsystem.controllers;

import examsystem.dtos.MyTestsDataDTO;
import examsystem.dtos.MyTestsDataForNewExamDTO;
import examsystem.dtos.QuestionDTO;
import examsystem.dtos.TestDTO;
import examsystem.services.TestService;
import examsystem.services.TestTimerTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Wojciech on 26.09.2015.
 */
@Controller
@RequestMapping("/testManager")
public class TestController {

    @Autowired
    TestService testService;



    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String index() {
        return "teacher/newtest::content";
    }

    @RequestMapping(value="/show", method = RequestMethod.GET)
    public String show() {
        return "teacher/mytests::content";
    }

    @RequestMapping(value="saveTestOptions", method = RequestMethod.POST, produces="text/plain")
    @ResponseBody
    public String saveTestOptions(@RequestBody @Valid TestDTO testDTO) {
        return testService.save(testDTO);
    }

    @RequestMapping(value="saveNewQuestion", method = RequestMethod.POST, produces="text/plain")
    @ResponseBody
    public String saveNewQuestion(@RequestBody @Valid QuestionDTO questionDTO) {
        return testService.saveQuestionForTestId(questionDTO);
    }

    @RequestMapping(value="findMyTests", method = RequestMethod.GET)
     @ResponseBody
     public List<MyTestsDataDTO> findMyTests() {
        return testService.findMyTestsData();
    }

    @RequestMapping(value="findMyTests/newexam", method = RequestMethod.GET)
    @ResponseBody
    public List<MyTestsDataForNewExamDTO> findMyTestsForNewExam() {
        return testService.findMyTestsDataForNewExam();
    }

    @RequestMapping(value="removeTest/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void findMyTests(@PathVariable String id) {
        testService.removeTestById(id);
    }
}
