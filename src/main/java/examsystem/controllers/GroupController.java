package examsystem.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Wojciech on 18.10.2015.
 */
@Controller
@RequestMapping("/groups")
public class GroupController {
    @RequestMapping(value="/show", method = RequestMethod.GET)
    public String index() {
        return "groups";
    }
}
