package examsystem.DAOs;

import examsystem.entities.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Wojciech on 11.10.2015.
 */
public interface CustomUserDAO extends MongoRepository<UserEntity,String> {
    UserEntity findByEmail(String email);

    UserEntity findByAlbumNumber(Number albumNumber);
}
