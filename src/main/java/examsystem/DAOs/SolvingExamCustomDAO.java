package examsystem.DAOs;

import examsystem.dtos.SolvingExamsDTO;
import examsystem.entities.SolvingExamEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 29.10.2015.
 */
public interface SolvingExamCustomDAO extends MongoRepository<SolvingExamEntity,String> {
    SolvingExamEntity findByExamIdAndUserId(String examId, String userId);

    SolvingExamEntity findByUserId(String id);
}
