package examsystem.DAOs;

import examsystem.entities.ExamEntity;
import examsystem.entities.TestEntity;
import examsystem.entities.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 22.10.2015.
 */
public interface CustomExamDAO extends MongoRepository<ExamEntity,String> {
    List<ExamEntity> findByTestId(Set<String> testIds);
}
