package examsystem.DAOs;

import examsystem.dtos.UserAnswerForSolvingEntity;
import examsystem.entities.SolvingExamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Wojciech on 29.10.2015.
 */
@Repository
public class SolvingExamDAO implements CustomDAO<SolvingExamCustomDAO>{

    @Autowired
    SolvingExamCustomDAO solvingExamCustomDAO;


    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public SolvingExamCustomDAO getCustom() {
        return solvingExamCustomDAO;
    }

    public List<SolvingExamEntity> findByExamIdsAndUserId(String userId, Set<String> examsIds) {
        Query query = new Query(Criteria.where("userId").is(userId).and("examId").in(examsIds));
        return mongoTemplate.find(query, SolvingExamEntity.class);
    }

    public SolvingExamEntity findByUserIdAndIsSolving(String userId, boolean isSolving) {
        Query query = new Query(Criteria.where("userId").is(userId).and("isSolving").in(isSolving));
        return mongoTemplate.findOne(query, SolvingExamEntity.class);
    }

    public void incrementActualQuestionById(String id, Integer incrementNumber) {
        Query query = new Query(Criteria.where("id").is(id).and("isSolving").in(true));
        Update update = new Update().inc("actualQuestionNumber",incrementNumber);
        mongoTemplate.updateFirst(query, update, SolvingExamEntity.class);
    }

    public Integer findActualQuestionNumberById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        query.fields().include("actualQuestionNumber");
        return mongoTemplate.findOne(query, SolvingExamEntity.class).getActualQuestionNumber();
    }

    public Map<String,UserAnswerForSolvingEntity> findAllAnswerIdsById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        query.fields().include("questionPoolIdsWithAnswers");
        return mongoTemplate.findOne(query, SolvingExamEntity.class).getQuestionPoolIdsWithAnswers();
    }

    public void updateAnswer(String solvingExamId, String questionId, UserAnswerForSolvingEntity userAnswerForSolvingEntity) {
        Query query = new Query(Criteria.where("id").is(solvingExamId));

        Map<String, UserAnswerForSolvingEntity> allAnswerIdsById = findAllAnswerIdsById(solvingExamId);
        allAnswerIdsById.put(questionId, userAnswerForSolvingEntity);

        Update update = new Update().set("questionPoolIdsWithAnswers", allAnswerIdsById);
        mongoTemplate.updateFirst(query, update, SolvingExamEntity.class);
    }

    public UserAnswerForSolvingEntity findAnswerByIdAndQuestionId(String solvingExamId, String questionId) {
        Query query = new Query(Criteria.where("id").is(solvingExamId));
        query.fields().include("questionPoolIdsWithAnswers");
        return mongoTemplate.findOne(query,SolvingExamEntity.class).getQuestionPoolIdsWithAnswers().get(questionId);
    }

    public void updateEndedById(String solvingExamId) {
        Query query = new Query(Criteria.where("id").is(solvingExamId));
        Update update = new Update().set("ended", true).set("isSolving", false);
        mongoTemplate.updateFirst(query, update, SolvingExamEntity.class);

    }

    public String findQuestioAnswerByIdAndQuestionId(String solvingExamId, String questionId) {
        Query query = new Query(Criteria.where("id").is(solvingExamId));
        query.fields().include("questionPoolIdsWithAnswers."+questionId);
        return mongoTemplate.findOne(query,SolvingExamEntity.class).getQuestionPoolIdsWithAnswers().get(questionId).getAnswerTextOrKey();

    }

    public Date findStartExamTimeByExamIdAndUserId(String examId, String userId) {
        Query query = new Query(Criteria.where("examId").is(examId));
        query.fields().include("startDate");
        return mongoTemplate.findOne(query,SolvingExamEntity.class).getStartDate();
    }
}
