package examsystem.DAOs;

import examsystem.dtos.TestDTO;
import examsystem.entities.TestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 07.10.2015.
 */
@Repository
public class TestDAO {

    @Autowired
    TestCustomDAO testCustomDAO;

    @Autowired
    MongoTemplate mongoTemplate;

    public TestCustomDAO getCustom(){
        return testCustomDAO;
    }

    public void setTestAsRemoveByIdAndUserId(String testId, String userId) {
        Query query = new Query(Criteria.where("id").is(testId).and("userId").is(userId));
        Update update = new Update().set("remove", true);
        mongoTemplate.updateFirst(query,update,TestEntity.class);
    }

    public List<TestEntity> findByUserIds(Set<String> myTeacherAccessIds) {
        Query query = new Query(Criteria.where("userId").in(myTeacherAccessIds).and("remove").ne(true));
        List<TestEntity> testEntities = mongoTemplate.find(query, TestEntity.class);
        return testEntities;
    }
}
