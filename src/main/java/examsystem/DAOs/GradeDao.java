package examsystem.DAOs;

import com.mongodb.Mongo;
import examsystem.dtos.OpenQuestionGradeDTO;
import examsystem.entities.GradeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * Created by Wojciech on 20.11.2015.
 */
@Repository
public class GradeDao implements CustomDAO<CustomGradeDao>{

    @Autowired
    CustomGradeDao customGradeDao;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public CustomGradeDao getCustom() {
        return customGradeDao;
    }

    public void saveResultForOpenQuestion(OpenQuestionGradeDTO openQuestionGradeDTO) {
        Query query = new Query(Criteria.where("id").is(openQuestionGradeDTO.getGradeId()));
        Update update = new Update().set("questionPointsMap."+openQuestionGradeDTO.getQuestionId(),openQuestionGradeDTO.getGradePoints());
        mongoTemplate.updateFirst(query,update, GradeEntity.class);
    }
}
