package examsystem.DAOs;

import examsystem.entities.QuestionEntity;
import examsystem.entities.TestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Wojciech on 08.10.2015.
 */
public interface QuestionCustomDAO extends MongoRepository<QuestionEntity,String> {
    
}
