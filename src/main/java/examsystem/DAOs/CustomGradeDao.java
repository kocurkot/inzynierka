package examsystem.DAOs;

import examsystem.entities.GradeEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Wojciech on 20.11.2015.
 */
public interface CustomGradeDao extends MongoRepository<GradeEntity ,String> {
    List<GradeEntity> findByExamId(String examId);
    GradeEntity findBySolvingExamId(String solvingExamId);
    List<GradeEntity> findByStudentUserId(String loggedUserId);
}
