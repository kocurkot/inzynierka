package examsystem.DAOs;

import examsystem.entities.ExamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 22.10.2015.
 */
@Repository
public class ExamDAO implements CustomDAO<CustomExamDAO> {

    @Autowired
    CustomExamDAO customExamDAO;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public CustomExamDAO getCustom() {
        return customExamDAO;
    }

    public List<ExamEntity> findByTestIds(Set<String> testIds) {
        Query query = new Query(Criteria.where("testId").in(testIds).and("remove").ne(true));
        List<ExamEntity> examEntities = mongoTemplate.find(query,ExamEntity.class);
        return examEntities;
    }

    public void setAsRemoveByIdAndUserId(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        Update update = new Update().set("remove",true);
        mongoTemplate.updateFirst(query,update,ExamEntity.class);
    }

    public Long findQuestionCountById(String examId) {
        Query query = new Query(Criteria.where("id").is(examId));
        query.fields().include("questionsAmount");
        return mongoTemplate.findOne(query,ExamEntity.class).getQuestionsAmount();
    }
}
