package examsystem.DAOs;

import examsystem.entities.TestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 07.10.2015.
 */
public interface TestCustomDAO extends MongoRepository<TestEntity, String> {
    List<TestEntity> findByUserIdAndRemove(String userId, Boolean remove);
}
