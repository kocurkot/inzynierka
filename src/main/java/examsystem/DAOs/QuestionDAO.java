package examsystem.DAOs;

import examsystem.entities.QuestionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Repository;

/**
 * Created by Wojciech on 08.10.2015.
 */
@Repository
public class QuestionDAO implements CustomDAO<QuestionCustomDAO>{
    @Autowired QuestionCustomDAO questionCustomDAO;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public QuestionCustomDAO getCustom() {
        return questionCustomDAO;
    }

    public Long countQuestionsByTestId(String testId) {
        Query query = new Query(Criteria.where("testId").is(testId));
        return mongoTemplate.count(query,QuestionEntity.class);
    }
}
