package examsystem.DAOs;

import examsystem.dtos.UserDTO;
import examsystem.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 19.10.2015.
 */
@Repository
public class UserDAO {

    @Autowired
    CustomUserDAO customUserDAO;

    @Autowired
    MongoTemplate mongoTemplate;

    public CustomUserDAO getCustom(){
        return customUserDAO;
    }

    public void saveStudentAccessPassword(String studentAccessPassword, String userId) {
        Query query = new Query(Criteria.where("id").is(userId));
        Update update = new Update().set("accessForStudentsPassword", studentAccessPassword);
        mongoTemplate.updateFirst(query,update, UserEntity.class);
    }

    public void updateMyTeachersAccess(String studentId, String teacherId) {
        Query query = new Query(Criteria.where("id").is(studentId));
        Update update = new Update().addToSet("myTeacherAccessIds", teacherId);
        mongoTemplate.updateFirst(query,update, UserEntity.class);
    }

    public List<UserEntity> findEmailAndNameByUserIds(Set<String> myTeacherAccessIds) {
        Query query = new Query(Criteria.where("id").in(myTeacherAccessIds));
        query.fields().include("email").include("firstName").include("lastName").exclude("id");
        return mongoTemplate.find(query,UserEntity.class);
    }

    public void saveNewPasswordByUserId(String newPassword1, String id) {
        Query query = new Query(Criteria.where("id").is(id));
        Update update = new Update().set("password", newPassword1);
        mongoTemplate.updateFirst(query,update, UserEntity.class);
    }
}
