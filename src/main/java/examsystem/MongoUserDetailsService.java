package examsystem;

import examsystem.entities.UserEntity;
import examsystem.exceptions.UserNotFoundException;
import examsystem.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Service
public class MongoUserDetailsService implements UserDetailsService {

    private Logger logger = Logger.getLogger(String.valueOf(this));

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("User with email: '" + username + "' try login to System");
        UserEntity userEntity = null;

        try{
            userEntity = userService.findUserByEmail(username);
            logger.info("User details" + userEntity.toString());
        }catch(UserNotFoundException e){
            throw new UsernameNotFoundException("User " + username + " does not exist");
        }

        return getUserDetails(userEntity);
    }

    private UserDetails getUserDetails(UserEntity userEntity) {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        List<GrantedAuthority> authorities = new ArrayList<>();
        userEntity.getRoles().stream().forEach(p -> authorities.add(
                new SimpleGrantedAuthority(p)
        ));
        String userId = userEntity.getId();

        return new CustomUser(
                userEntity.getEmail(),
                userEntity.getPassword(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                authorities,
                userId
        );
    }
}
