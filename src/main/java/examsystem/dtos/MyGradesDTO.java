package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * Created by Wojciech on 23.11.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class MyGradesDTO {
    private String name;
    private Date startTime;
    private Number gradePoints;
    private Number gradeMaxPoints;
    private Boolean needToGradeOpenAnswer;

    public Boolean getNeedToGradeOpenAnswer() {
        return needToGradeOpenAnswer;
    }

    public void setNeedToGradeOpenAnswer(Boolean needToGradeOpenAnswer) {
        this.needToGradeOpenAnswer = needToGradeOpenAnswer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Number getGradePoints() {
        return gradePoints;
    }

    public void setGradePoints(Number gradePoints) {
        this.gradePoints = gradePoints;
    }

    public Number getGradeMaxPoints() {
        return gradeMaxPoints;
    }

    public void setGradeMaxPoints(Number gradeMaxPoints) {
        this.gradeMaxPoints = gradeMaxPoints;
    }
}
