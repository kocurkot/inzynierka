package examsystem.dtos;

/**
 * Created by Wojciech on 04.10.2015.
 */
public class Answer {
    private String text;
    private Boolean isProper;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getIsProper() {
        return isProper;
    }

    public void setIsProper(Boolean isProper) {
        this.isProper = isProper;
    }
}
