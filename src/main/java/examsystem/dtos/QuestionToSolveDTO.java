package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Wojciech on 12.11.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class QuestionToSolveDTO {
    private String id;

    @NotNull
    private String text;
    @NotNull
    private QuestionType type;
    private List<AnswerToSolve> closeAnswers;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<AnswerToSolve> getCloseAnswers() {
        return closeAnswers;
    }

    public void setCloseAnswers(List<AnswerToSolve> closeAnswers) {
        this.closeAnswers = closeAnswers;
    }

}
