package examsystem.dtos;

import java.util.Map;

/**
 * Created by Wojciech on 14.12.2015.
 */
public class MyTestsDataForNewExamDTO extends TestDTO{
    private Map<Number, Number> questionGroupedCount;

    public Map<Number, Number> getQuestionGroupedCount() {
        return questionGroupedCount;
    }

    public void setQuestionGroupedCount(Map<Number, Number> questionGroupedCount) {
        this.questionGroupedCount = questionGroupedCount;
    }
}
