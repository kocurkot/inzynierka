package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 22.09.2015.
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class UserDTO {
    private String id;
    private String firstName;
    private String lastName;
    @NotNull
    @Email
    private String email;
    private List<String> roles;
    private String accessForStudentsPassword;
    private Set<String> myTeacherAccessIds;
    private Number albumNumber;

    public Number getAlbumNumber() {
        return albumNumber;
    }

    public void setAlbumNumber(Number albumNumber) {
        this.albumNumber = albumNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccessForStudentsPassword() {
        return accessForStudentsPassword;
    }

    public void setAccessForStudentsPassword(String accessForStudentsPassword) {
        this.accessForStudentsPassword = accessForStudentsPassword;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Set<String> getMyTeacherAccessIds() {
        return myTeacherAccessIds;
    }

    public void setMyTeacherAccessIds(Set<String> myTeacherAccessIds) {
        this.myTeacherAccessIds = myTeacherAccessIds;
    }

}
