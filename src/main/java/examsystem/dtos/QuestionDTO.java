package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Wojciech on 27.09.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class QuestionDTO {

    private String id;

    @NotNull
    private String text;
    @NotNull
    private QuestionType type;
    private List<Answer> closeAnswers;
    private List<String> keyAnswers;
    private String testId;
    private Number points;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<Answer> getCloseAnswers() {
        return closeAnswers;
    }

    public void setCloseAnswers(List<Answer> closeAnswers) {
        this.closeAnswers = closeAnswers;
    }

    public List<String> getKeyAnswers() {
        return keyAnswers;
    }

    public void setKeyAnswers(List<String> keyAnswers) {
        this.keyAnswers = keyAnswers;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Number getPoints() {
        return points;
    }

    public void setPoints(Number points) {
        this.points = points;
    }

    @AssertTrue
    private boolean isValid() {
        if(this.type.equals(QuestionType.OPEN)){
            return this.closeAnswers == null && this.keyAnswers == null;
        }
        else if(this.type.equals(QuestionType.CLOSE)){
            return this.closeAnswers != null && this.keyAnswers == null
                    && !this.closeAnswers.isEmpty();
        }
        else if(this.type.equals(QuestionType.KEY)){
            return this.closeAnswers == null && this.keyAnswers != null
                    && !this.keyAnswers.isEmpty();
        }
        else{
            return false;
        }
    }
}
