package examsystem.dtos;

import java.util.List;

/**
 * Created by Wojciech on 12.11.2015.
 */
public class UserAnswerDTO {
    private String solvingExamId;
    private String questionId;
    private List<AnswerToSolve> closeAnswers;
    private String textAnswer;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<AnswerToSolve> getCloseAnswers() {
        return closeAnswers;
    }

    public void setCloseAnswers(List<AnswerToSolve> closeAnswers) {
        this.closeAnswers = closeAnswers;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }

    public String getSolvingExamId() {
        return solvingExamId;
    }

    public void setSolvingExamId(String solvingExamId) {
        this.solvingExamId = solvingExamId;
    }
}
