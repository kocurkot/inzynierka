package examsystem.dtos;

/**
 * Created by Wojciech on 22.11.2015.
 */
public class OpenQuestionGradeDTO {
    private String questionId;
    private String gradeId;
    private Number gradePoints;

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public Number getGradePoints() {
        return gradePoints;
    }

    public void setGradePoints(Number gradePoints) {
        this.gradePoints = gradePoints;
    }

    public String getQuestionId() {

        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
