package examsystem.dtos;

/**
 * Created by Wojciech on 20.10.2015.
 */
public class MyTestsDataDTO extends TestDTO {
    private Long questionsNumber;

    public Long getQuestionsNumber() {
        return questionsNumber;
    }

    public void setQuestionsNumber(Long questionsNumber) {
        this.questionsNumber = questionsNumber;
    }
}
