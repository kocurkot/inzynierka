package examsystem.dtos;

import javax.validation.constraints.NotNull;

/**
 * Created by Wojciech on 17.12.2015.
 */
public class ChangePasswordDto {
    @NotNull
    private String oldPassword;
    @NotNull
    private String newPassword1;
    @NotNull
    private String newPassword2;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword1() {
        return newPassword1;
    }

    public void setNewPassword1(String newPassword1) {
        this.newPassword1 = newPassword1;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }
}
