package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;

/**
 * Created by Wojciech on 20.10.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ExamDTO {

    private String id;
    @NotNull
    private String name;
    @NotNull
    private String password;
    @NotNull
    private Date startDate;
    @NotNull
    private Date endDate;
    @NotNull
    private String testId;
    @NotNull
    @Min(1)
    private Long durationSeconds;
    @NotNull
    @Min(1)
    private Long questionsAmount;
    private Boolean remove;

    private Map<String, String> questionGroupedCount;

    public Map<String, String> getQuestionGroupedCount() {
        return questionGroupedCount;
    }

    public void setQuestionGroupedCount(Map<String, String> questionGroupedCount) {
        this.questionGroupedCount = questionGroupedCount;
    }

    public Boolean getRemove() {
        return remove;
    }

    public void setRemove(Boolean remove) {
        this.remove = remove;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Long getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(Long durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public Long getQuestionsAmount() {
        return questionsAmount;
    }

    public void setQuestionsAmount(Long questionsAmount) {
        this.questionsAmount = questionsAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @AssertTrue
    private boolean isValid() {
        if(this.getStartDate().before(this.getEndDate())){
            return true;
        }
//        String message = "bad exam dates";
//        logger.info(message);
        return false;
    }
}
