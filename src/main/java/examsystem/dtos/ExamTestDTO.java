package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Wojciech on 08.11.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)

public class ExamTestDTO {
    private ExamDTO examDTO;
    private TestDTO testDTO;

    public ExamDTO getExamDTO() {
        return examDTO;
    }

    public void setExamDTO(ExamDTO examDTO) {
        this.examDTO = examDTO;
    }

    public TestDTO getTestDTO() {
        return testDTO;
    }

    public void setTestDTO(TestDTO testDTO) {
        this.testDTO = testDTO;
    }
}
