package examsystem.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Wojciech on 21.11.2015.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class GradeForExamTableDTO {
    private String name;
    private String surname;
    private String mail;
    private Double grade;
    private String solvingExamId;
    private Double maxGrade;
    private Boolean needToGradeOpenAnswer;

    public Boolean getNeedToGradeOpenAnswer() {
        return needToGradeOpenAnswer;
    }

    public void setNeedToGradeOpenAnswer(Boolean needToGradeOpenAnswer) {
        this.needToGradeOpenAnswer = needToGradeOpenAnswer;
    }

    public Double getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Double maxGrade) {
        this.maxGrade = maxGrade;
    }

    public String getSolvingExamId() {
        return solvingExamId;
    }

    public void setSolvingExamId(String solvingExamId) {
        this.solvingExamId = solvingExamId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
