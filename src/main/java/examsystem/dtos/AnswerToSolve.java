package examsystem.dtos;

/**
 * Created by Wojciech on 12.11.2015.
 */
public class AnswerToSolve extends Answer{

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
