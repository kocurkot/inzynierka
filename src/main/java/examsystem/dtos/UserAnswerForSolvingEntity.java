package examsystem.dtos;

import java.util.List;
import java.util.Map;

/**
 * Created by Wojciech on 12.11.2015.
 */
public class UserAnswerForSolvingEntity {
    private String answerTextOrKey;
    private List<AnswerToSolve> closeAnswers;

    public String getAnswerTextOrKey() {
        return answerTextOrKey;
    }

    public void setAnswerTextOrKey(String answerTextOrKey) {
        this.answerTextOrKey = answerTextOrKey;
    }

    public List<AnswerToSolve> getCloseAnswers() {
        return closeAnswers;
    }

    public void setCloseAnswers(List<AnswerToSolve> closeAnswers) {
        this.closeAnswers = closeAnswers;
    }
}
