package examsystem.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

/**
 * Created by Wojciech on 20.11.2015.
 */
@Document(collection = "grade")
public class GradeEntity {
    @Id
    private String id;
    private String solvingExamId;
    private String studentUserId;
    private String teacherUserId;
    private String examId;
    private Map<String, Double> questionPointsMap;


    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSolvingExamId() {
        return solvingExamId;
    }

    public void setSolvingExamId(String solvingTestId) {
        this.solvingExamId = solvingTestId;
    }

    public String getStudentUserId() {
        return studentUserId;
    }

    public void setStudentUserId(String studentUserId) {
        this.studentUserId = studentUserId;
    }

    public String getTeacherUserId() {
        return teacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        this.teacherUserId = teacherUserId;
    }

    public Map<String, Double> getQuestionPointsMap() {
        return questionPointsMap;
    }

    public void setQuestionPointsMap(Map<String, Double> questionPointsMap) {
        this.questionPointsMap = questionPointsMap;
    }
}
