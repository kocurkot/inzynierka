package examsystem.entities;

import examsystem.dtos.Answer;
import examsystem.dtos.QuestionType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by Wojciech on 08.10.2015.
 */
@Document(collection = "questions")
public class QuestionEntity {
    @Id
    private String id;
    private String text;
    private QuestionType type;
    private List<Answer> closeAnswers;
    private List<String> keyAnswers;
    private String testId;
    private Number points;

    public Number getPoints() {
        return points;
    }

    public void setPoints(Number points) {
        this.points = points;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<Answer> getCloseAnswers() {
        return closeAnswers;
    }

    public void setCloseAnswers(List<Answer> closeAnswers) {
        this.closeAnswers = closeAnswers;
    }

    public List<String> getKeyAnswers() {
        return keyAnswers;
    }

    public void setKeyAnswers(List<String> keyAnswers) {
        this.keyAnswers = keyAnswers;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }
}
