package examsystem.entities;

import examsystem.dtos.UserAnswerForSolvingEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

/**
 * Created by Wojciech on 29.10.2015.
 */
@Document(collection = "solvingExams")
public class SolvingExamEntity {

    @Id
    private String id;
    private String examId;
    private String userId;
    private Boolean isSolving;
    private Map<String, UserAnswerForSolvingEntity> questionPoolIdsWithAnswers;
    private Integer actualQuestionNumber;
    private Boolean ended;
    private Date startDate;
//    private Set<String> questionPoolIds;


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getIsSolving() {
        return isSolving;
    }

    public void setIsSolving(Boolean isSolving) {
        this.isSolving = isSolving;
    }

    public Boolean getSolving() {
        return isSolving;
    }

    public void setSolving(Boolean solving) {
        isSolving = solving;
    }

    public Map<String, UserAnswerForSolvingEntity> getQuestionPoolIdsWithAnswers() {
        return questionPoolIdsWithAnswers;
    }

    public void setQuestionPoolIdsWithAnswers(Map<String, UserAnswerForSolvingEntity> questionPoolIdsWithAnswers) {
        this.questionPoolIdsWithAnswers = questionPoolIdsWithAnswers;
    }

    public Integer getActualQuestionNumber() {
        return actualQuestionNumber;
    }

    public void setActualQuestionNumber(Integer actualQuestionNumber) {
        this.actualQuestionNumber = actualQuestionNumber;
    }

    public Boolean getEnded() {
        return ended;
    }

    public void setEnded(Boolean ended) {
        this.ended = ended;
    }
}
