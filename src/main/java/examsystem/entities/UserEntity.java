package examsystem.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Document(collection = "users")
public class UserEntity {
    @Id
    private String id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private List<String> roles;
    @NotNull
    @Indexed(unique = true) @Email
    private String email;
    @NotNull
    @JsonIgnore
    private String password;

    private String accessForStudentsPassword;
    private Set<String> myTeacherAccessIds;

    private Number albumNumber;

    public Number getAlbumNumber() {
        return albumNumber;
    }

    public void setAlbumNumber(Number albumNumber) {
        this.albumNumber = albumNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getAccessForStudentsPassword() {
        return accessForStudentsPassword;
    }

    public void setAccessForStudentsPassword(String accessForStudentsPassword) {
        this.accessForStudentsPassword = accessForStudentsPassword;
    }

    public Set<String> getMyTeacherAccessIds() {
        return myTeacherAccessIds;
    }

    public void setMyTeacherAccessIds(Set<String> myTeacherAccessIds) {
        this.myTeacherAccessIds = myTeacherAccessIds;
    }
}
