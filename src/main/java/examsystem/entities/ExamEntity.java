package examsystem.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Wojciech on 20.10.2015.
 */
@Document(collection = "exams")
public class ExamEntity {
    @Id
    private String id;
    private String name;
    private String password;
    private Date startDate;
    private Date endDate;
    private String testId;
    private Long durationSeconds;
    private Long questionsAmount;
    private Boolean remove;
    private Map<String, String> questionGroupedCount;

    public Map<String, String> getQuestionGroupedCount() {
        return questionGroupedCount;
    }

    public void setQuestionGroupedCount(Map<String, String> questionGroupedCount) {
        this.questionGroupedCount = questionGroupedCount;
    }

    public Boolean getRemove() {
        return remove;
    }

    public void setRemove(Boolean remove) {
        this.remove = remove;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Long getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(Long durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public Long getQuestionsAmount() {
        return questionsAmount;
    }

    public void setQuestionsAmount(Long questionsAmount) {
        this.questionsAmount = questionsAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
