package examsystem.exceptions;

/**
 * Created by Wojciech on 19.10.2015.
 */
public class BadTeacherResourcesAccessData extends Exception {
    public BadTeacherResourcesAccessData() {
    }

    public BadTeacherResourcesAccessData(String message) {
        super(message);
    }

    public BadTeacherResourcesAccessData(String message, Throwable cause) {
        super(message, cause);
    }

    public BadTeacherResourcesAccessData(Throwable cause) {
        super(cause);
    }

    public BadTeacherResourcesAccessData(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
