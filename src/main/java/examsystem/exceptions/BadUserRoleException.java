package examsystem.exceptions;

/**
 * Created by Wojciech on 19.10.2015.
 */
public class BadUserRoleException extends Exception {
    public BadUserRoleException() {
    }

    public BadUserRoleException(String message) {
        super(message);
    }

    public BadUserRoleException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadUserRoleException(Throwable cause) {
        super(cause);
    }

    public BadUserRoleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
