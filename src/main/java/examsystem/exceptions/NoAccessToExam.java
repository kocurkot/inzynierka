package examsystem.exceptions;


/**
 * Created by Wojciech on 29.10.2015.
 */
public class NoAccessToExam extends Exception{
    public NoAccessToExam() {
    }

    public NoAccessToExam(String message) {
        super(message);
    }

    public NoAccessToExam(String message, Throwable cause) {
        super(message, cause);
    }

    public NoAccessToExam(Throwable cause) {
        super(cause);
    }

    public NoAccessToExam(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
