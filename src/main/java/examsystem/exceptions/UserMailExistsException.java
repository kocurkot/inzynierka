package examsystem.exceptions;

/**
 * Created by Wojciech on 17.12.2015.
 */
public class UserMailExistsException extends Exception {
    public UserMailExistsException() {
    }

    public UserMailExistsException(String message) {
        super(message);
    }

    public UserMailExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserMailExistsException(Throwable cause) {
        super(cause);
    }

    public UserMailExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
