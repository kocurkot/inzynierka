package examsystem.exceptions;

/**
 * Created by Wojciech on 17.12.2015.
 */
public class UserAlbumNumberExistsException extends Exception {
    public UserAlbumNumberExistsException() {
    }

    public UserAlbumNumberExistsException(String message) {
        super(message);
    }

    public UserAlbumNumberExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAlbumNumberExistsException(Throwable cause) {
        super(cause);
    }

    public UserAlbumNumberExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
