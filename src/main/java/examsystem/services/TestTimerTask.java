package examsystem.services;

import examsystem.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Wojciech on 29.10.2015.
 */
public class TestTimerTask implements Runnable {

    private SimpMessagingTemplate template;
    private String userName;
    private Long durationSeconds;
    private LocalDateTime startDate;
    private String solvingExamId;
    private SolvingExamService solvingExamService;


    public TestTimerTask(SimpMessagingTemplate template, String userName, Long durationSeconds, LocalDateTime startDate, String solvingExamId, SolvingExamService solvingExamService) {
        this.template = template;
        this.userName = userName;
        this.durationSeconds = durationSeconds;
        this.startDate = startDate;
        this.solvingExamId = solvingExamId;
        this.solvingExamService = solvingExamService;
    }

    @Override
    public void run() {
        Long secondsBetweenDates = Duration.between(startDate,LocalDateTime.now()).getSeconds();
        Long remainingTime = durationSeconds - secondsBetweenDates;

        if(userName != null){

            template.convertAndSendToUser(userName, "/topic/greetings/"+solvingExamId, remainingTime);
        }

        if(remainingTime <= 0){
            solvingExamService.exitExam(solvingExamId);
        }
    }


}
