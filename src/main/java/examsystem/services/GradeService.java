package examsystem.services;

import examsystem.DAOs.GradeDao;
import examsystem.DAOs.SolvingExamDAO;
import examsystem.dtos.*;
import examsystem.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Wojciech on 20.11.2015.
 */
@Service
public class GradeService {

    @Autowired
    SolvingExamService solvingExamService;


    @Autowired
    SolvingExamDAO solvingExamDAO;

    @Autowired
    UserService userService;

    @Autowired
    ExamService examService;

    @Autowired
    TestService testService;

    @Autowired
    QuestionsService questionsService;

    @Autowired
    GradeDao gradeDao;

    public void gradeExam(String solvingExamId){

        SolvingExamEntity solvingExamEntity = solvingExamDAO.getCustom().findOne(solvingExamId);
        ExamEntity examEntity = examService.getExamById(solvingExamEntity.getExamId());
        TestEntity testEntity = testService.findById(examEntity.getTestId());

        String userId = solvingExamEntity.getUserId();

        GradeEntity gradeEntity = new GradeEntity();
        gradeEntity.setSolvingExamId(solvingExamId);
        gradeEntity.setStudentUserId(userId);
        gradeEntity.setTeacherUserId(testEntity.getUserId());
        gradeEntity.setExamId(examEntity.getId());


        Map<String,Double> questionPointsMap = new HashMap<>();

        Map<String, UserAnswerForSolvingEntity> questionPoolIdsWithAnswers = solvingExamEntity.getQuestionPoolIdsWithAnswers();

        questionPoolIdsWithAnswers.keySet().stream().forEach(questionId -> {

            QuestionEntity questionEntityById = questionsService.findQuestionEntityById(questionId);
            UserAnswerForSolvingEntity myAnswer = questionPoolIdsWithAnswers.get(questionId);

            Double points = 1.0;

            switch(questionEntityById.getType()){
                case CLOSE:

                    Boolean goodAnswer = true;

                    int i = 1;
                    for(Answer answer : questionEntityById.getCloseAnswers()){
                        int index = i;
                        AnswerToSolve myCloseAnswer = myAnswer.getCloseAnswers()
                                .stream()
                                .filter(p -> p.getId() == index)
                                .collect(Collectors.toList()).get(0);
                        if(!myCloseAnswer.getIsProper().equals(answer.getIsProper())){
                            goodAnswer = false;
                        }
                        i++;
                    }

                    if(goodAnswer){
                        questionPointsMap.put(questionId,points);
                    }else{
                        questionPointsMap.put(questionId,0d);
                    }



                    break;
                case OPEN:
                    //cannot check automatically, it will check by teacher later
                    questionPointsMap.put(questionId,null);
                    break;
                case KEY:

                    if(questionEntityById.getKeyAnswers().contains(myAnswer.getAnswerTextOrKey())){
                        questionPointsMap.put(questionId,points);
                    }else{
                        questionPointsMap.put(questionId,0d);
                    }

                    break;
            }



        });

        gradeEntity.setQuestionPointsMap(questionPointsMap);

        gradeDao.getCustom().save(gradeEntity);

    }

    public List<GradeForExamTableDTO> findGradesByExamId(String examId) {

        List<GradeForExamTableDTO> gradeForExamTableDTOs = new ArrayList<>();
        List<GradeEntity> gradeEntities = gradeDao.getCustom().findByExamId(examId);

        gradeEntities.stream().forEach(p ->{
            GradeForExamTableDTO gradeForExamTableDTO = new GradeForExamTableDTO();
            UserEntity userEntity = userService.findUserById(p.getStudentUserId());
            gradeForExamTableDTO.setName(userEntity.getFirstName());
            gradeForExamTableDTO.setSurname(userEntity.getLastName());
            gradeForExamTableDTO.setMail(userEntity.getEmail());

            gradeForExamTableDTO.setSolvingExamId(p.getSolvingExamId());
            gradeForExamTableDTO.setMaxGrade(50.0);
            gradeForExamTableDTO.setNeedToGradeOpenAnswer(false);

            Double sumPoints = 0.0;
            for(Double gradeForQuestion : p.getQuestionPointsMap().values()){
                if(gradeForQuestion != null){
                    sumPoints += gradeForQuestion;
                }else{
                    gradeForExamTableDTO.setNeedToGradeOpenAnswer(true);
                }
            }
            gradeForExamTableDTO.setGrade(sumPoints);
            gradeForExamTableDTOs.add(gradeForExamTableDTO);



        });

        return gradeForExamTableDTOs;
    }

    public List<GradeForExamTableDTO> findExamToGrade() {
        List<GradeForExamTableDTO> gradeForExamTableDTOs = new ArrayList<>();

       // gradeDao

        return gradeForExamTableDTOs;
    }

    public List<QuestionsToGradeDTO> findOpenQuestionsToGrade(String solvingExamId) {

        List<QuestionsToGradeDTO> questionsToGradeDTOs = new ArrayList<>();
        GradeEntity gradeEntity = gradeDao.getCustom().findBySolvingExamId(solvingExamId);
        Map<String, Double> questionPointsMap = gradeEntity.getQuestionPointsMap();


        questionPointsMap.keySet().stream().filter(p->questionPointsMap.get(p) == null).forEach(p->{

            QuestionsToGradeDTO questionsToGradeDTO = new QuestionsToGradeDTO();
            QuestionEntity questionEntity = questionsService.findQuestionEntityById(p);
            questionsToGradeDTO.setQuestionId(p);
            questionsToGradeDTO.setGradeId(gradeEntity.getId());
            questionsToGradeDTO.setQuestionText(questionEntity.getText());
            String answerText = solvingExamService.findOpenQuestionAnswerBySolvingExamIdAndQuestionId(gradeEntity.getSolvingExamId(),p);
            questionsToGradeDTO.setQuestionAnswer(answerText);
            questionsToGradeDTO.setPoints(questionEntity.getPoints());
            questionsToGradeDTOs.add(questionsToGradeDTO);
        });

        return questionsToGradeDTOs;
    }

    public void saveResultForOpenQuestion(OpenQuestionGradeDTO openQuestionGradeDTO) {
        gradeDao.saveResultForOpenQuestion(openQuestionGradeDTO);
    }

    public List<MyGradesDTO> findMyGrades() {
        List<MyGradesDTO> myGradesDTOs = new ArrayList<>();
        String loggedUserId = userService.getLoggedUser().getId();
        List<GradeEntity> gradeEntities = gradeDao.getCustom().findByStudentUserId(loggedUserId);



        gradeEntities.stream().forEach(p->{
            MyGradesDTO myGradesDTO = new MyGradesDTO();
            ExamEntity examById = examService.getExamById(p.getExamId());
            TestEntity testEntity = testService.findById(examById.getTestId());
            myGradesDTO.setName(examById.getName() + " -> ("+ testEntity.getName()+")");
            myGradesDTO.setGradeMaxPoints(examService.findQuestionsMaxPointsByExamEntity(examById));
            Date startTime = solvingExamService.findStartExamTimeByExamIdAndUserId(examById.getId(), loggedUserId);
            myGradesDTO.setStartTime(startTime);
            myGradesDTO.setNeedToGradeOpenAnswer(false);
            Double sumPoints = 0.0;
            for(Double gradeForQuestion : p.getQuestionPointsMap().values()){
                if(gradeForQuestion != null){
                    sumPoints += gradeForQuestion;
                }else{
                    myGradesDTO.setNeedToGradeOpenAnswer(true);
                }
            }
            myGradesDTO.setGradePoints(sumPoints);
            myGradesDTOs.add(myGradesDTO);
        });
        return myGradesDTOs;
    }
}
