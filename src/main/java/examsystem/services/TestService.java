package examsystem.services;

import examsystem.DAOs.QuestionDAO;
import examsystem.DAOs.TestDAO;
import examsystem.dtos.*;
import examsystem.entities.QuestionEntity;
import examsystem.entities.TestEntity;
import examsystem.services.mappers.QuestionMapper;
import examsystem.services.mappers.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Wojciech on 07.10.2015.
 */
@Service
public class TestService {

    @Autowired
    private TestDAO testDAO;

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private QuestionDAO questionDAO;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private UserService userService;

    @Autowired
    QuestionsService questionsService;

    public String save(TestDTO testDTO) {
        TestEntity testEntity = testMapper.getDocument(testDTO);
        UserDTO userDTO = userService.getLoggedUser();
        testEntity.setUserId(userDTO.getId());
        testEntity.setRemove(false);
        return testDAO.getCustom().save(testEntity).getId();
    }

    public String saveQuestionForTestId(QuestionDTO questionDTO) {
        return questionDAO.getCustom().save(questionMapper.getDocument(questionDTO))
                .getId();
    }

    public List<MyTestsDataDTO> findMyTestsData() {
        String userId = userService.getLoggedUser().getId();
        List<TestEntity> testEntities = testDAO.getCustom().findByUserIdAndRemove(userId, false);
        List<MyTestsDataDTO> myTestsDataDTOs = new ArrayList<>();

        for(TestDTO testDTO : testMapper.toDtos(testEntities)){
            MyTestsDataDTO myTestsDataDTO = new MyTestsDataDTO();
            String testId = testDTO.getId();
            myTestsDataDTO.setId(testId);
            myTestsDataDTO.setName(testDTO.getName());

            Long questionNumber = questionsService.countQuestionsForTestId(testId);

            myTestsDataDTO.setQuestionsNumber(questionNumber);

            myTestsDataDTOs.add(myTestsDataDTO);
        }
        return myTestsDataDTOs;
    }

    public void removeTestById(String testId) {
        String userId = userService.getLoggedUser().getId();
        testDAO.setTestAsRemoveByIdAndUserId(testId,userId);
    }

    public Set<String> findTestIdsByTeacherIds(Set<String> myTeacherAccessIds) {
        List<TestEntity> testEntities = testDAO.findByUserIds(myTeacherAccessIds);
        Set<String> testIds = new HashSet<>();
        testEntities.forEach(p -> testIds.add(p.getId()));
        return testIds;
    }

    public List<TestDTO> findTestsByTeacherIds(Set<String> myTeacherAccessIds) {
        List<TestEntity> testEntities = testDAO.findByUserIds(myTeacherAccessIds);
        return testMapper.toDtos(testEntities);
    }

    public TestEntity findById(String testId) {
        return testDAO.getCustom().findOne(testId);
    }

    public List<MyTestsDataForNewExamDTO> findMyTestsDataForNewExam() {
        List<MyTestsDataForNewExamDTO> myTestsDataForNewExamDTOs = new ArrayList<>();
        List<MyTestsDataDTO> myTestsData = findMyTestsData();

        myTestsData.stream().forEach(p->{
            MyTestsDataForNewExamDTO myTestsDataForNewExamDTO = new MyTestsDataForNewExamDTO();
            Map<Number, Number> questionPointsKeyQuestionCountValueMap = new HashMap<>();
            myTestsDataForNewExamDTO.setName(p.getName());
            myTestsDataForNewExamDTO.setId(p.getId());
            List<QuestionEntity> questions = questionsService.findQuestionsByTestId(p.getId());
            questions.stream().forEach(q->{
               if(questionPointsKeyQuestionCountValueMap.containsKey(q.getPoints())) {
                   int currentCount = questionPointsKeyQuestionCountValueMap.get(q.getPoints()).intValue();
                   currentCount++;
                   questionPointsKeyQuestionCountValueMap.put(q.getPoints(),currentCount);
               }else{
                   questionPointsKeyQuestionCountValueMap.put(q.getPoints(),1);
               }
            });
            myTestsDataForNewExamDTO.setQuestionGroupedCount(questionPointsKeyQuestionCountValueMap);
            myTestsDataForNewExamDTOs.add(myTestsDataForNewExamDTO);
        });

        return myTestsDataForNewExamDTOs;
    }
}
