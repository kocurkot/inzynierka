package examsystem.services;

import examsystem.DAOs.SolvingExamDAO;
import examsystem.DAOs.TestDAO;
import examsystem.dtos.*;
import examsystem.entities.ExamEntity;
import examsystem.entities.SolvingExamEntity;
import examsystem.entities.TestEntity;
import examsystem.services.mappers.SolvingExamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.ManagedMap;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

/**
 * Created by Wojciech on 29.10.2015.
 */
@Service
public class SolvingExamService {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private ExamService examService;

    @Autowired
    private SolvingExamDAO solvingExamDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private TestDAO testDAO;

    @Autowired
    private SolvingExamMapper solvingExamMapper;

    @Autowired
    private TestService testService;

    @Autowired
    private QuestionsService questionsService;

    @Autowired
    private GradeService gradeService;

    private ScheduledFuture<?> scheduledFuture;

    public void onConnectToSolvingTest() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User: " + userName + " connected to solving test websocket");
    }

    public String initSolvingData(String examId) {

        List<SolvingExamsDTO> solvingExamByExamId = getSolvingExamByExamIds(new HashSet<>(Arrays.asList(examId)));
        if (!solvingExamByExamId.isEmpty()) {
            return solvingExamByExamId.get(0).getId();
        }

        ExamEntity examEntity = examService.getExamById(examId);

        String userId = userService.getLoggedUser().getId();

        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        Long durationSeconds = examEntity.getDurationSeconds();

        Map<String, UserAnswerForSolvingEntity> questionPoolIdsWithAnswers = generateQuestionPoolIds(examEntity);

        SolvingExamEntity solvingExamEntity = new SolvingExamEntity();
        solvingExamEntity.setUserId(userId);
        solvingExamEntity.setExamId(examId);
        solvingExamEntity.setIsSolving(true);
        solvingExamEntity.setQuestionPoolIdsWithAnswers(questionPoolIdsWithAnswers);
        solvingExamEntity.setActualQuestionNumber(0);
        solvingExamEntity.setStartDate(new Date());

        String solvingExamId = solvingExamDAO.getCustom().save(solvingExamEntity).getId();
        scheduledFuture = taskScheduler.scheduleAtFixedRate(new TestTimerTask(template, userName, durationSeconds, LocalDateTime.now(), solvingExamId, this), 1000L);

        return solvingExamId;
    }

    private Map<String, UserAnswerForSolvingEntity> generateQuestionPoolIds(ExamEntity examEntity) {
        TestEntity testEntity = testService.findById(examEntity.getTestId());
        String testId = testEntity.getId();

        Map<String, String> questionGroupedCount = examEntity.getQuestionGroupedCount();

        int examMaxPoints = examService.findQuestionsMaxPointsByExamEntity(examEntity);



        Map<String, Integer> allQuestionIdsAndPoints =
                questionsService.findQuestionIdsAndPointsByTestId(testId);

        List<String> allQuestionIds = new ArrayList<>();
        allQuestionIds.addAll(allQuestionIdsAndPoints.keySet());

        List<String> randomSet = new ArrayList<>();

        Collections.shuffle(allQuestionIds);


        for (String points : questionGroupedCount.keySet()) {
            int pointsInt = Integer.parseInt(points);
            int amount = Integer.parseInt(questionGroupedCount.get(points));

            List<String> questionIdsForActualPoints = new ArrayList<>();

            for(String questionId : allQuestionIds){
                if(allQuestionIdsAndPoints.get(questionId).equals(pointsInt)){
                    questionIdsForActualPoints.add(questionId);
                }
            }
            Collections.shuffle(questionIdsForActualPoints);
            randomSet.addAll(questionIdsForActualPoints.subList(0, amount));
        }

        Collections.shuffle(randomSet);

        Map<String, UserAnswerForSolvingEntity> questionPoolIdsWithAnswers = new LinkedHashMap<>();
        //randomSet.forEach(p -> questionPoolIdsWithAnswers.put(p, null));

        for(String random : randomSet){
            questionPoolIdsWithAnswers.put(random, null);
        }

        return questionPoolIdsWithAnswers;

    }

    public Boolean isExamAvailableToSolve(String currentExamId) {
        ExamEntity currentExam = examService.getExamById(currentExamId);
        TestEntity testEntity = testDAO.getCustom().findOne(currentExam.getTestId());
        String testTeacherId = testEntity.getUserId();
        if (userService.findMyTeachersIds().contains(testTeacherId)) {
            //&& !isExamFinished(currentExam.getId())){
            return true;
        }
        return false;
    }

    public Boolean isExamFinished(String examId) {
        List<SolvingExamsDTO> solvingExamByExamId = getSolvingExamByExamIds(new HashSet<>(Arrays.asList(examId)));

        if ((solvingExamByExamId.isEmpty() || solvingExamByExamId.get(0).getEnded() == null)) {
            return false;
        }
        if ((solvingExamByExamId.get(0).getEnded() != null && !solvingExamByExamId.get(0).getEnded())) {
            return false;
        }


        return true;
    }

    private Boolean isExamStarted(String examId) {

        if (solvingExamDAO.getCustom().findByExamIdAndUserId(examId, userService.getLoggedUser().getId()) != null) {
            return true;
        }
        return false;
    }

    private Boolean isAnyExamSolving() {
        if (solvingExamDAO.findByUserIdAndIsSolving(userService.getLoggedUser().getId(), true) != null) {
            return true;
        }
        return false;
    }

    public Boolean isExamAuthorized(String examId, String password) {
        ExamEntity examEntity = examService.getExamById(examId);
        if (examEntity.getPassword().equals(password) && !isExamFinished(examId)) {
            return true;
        }
        return false;
    }

    public List<SolvingExamsDTO> getSolvingExamByExamIds(Set<String> examsIds) {
        String userId = userService.getLoggedUser().getId();
        List<SolvingExamsDTO> solvingExamsDTOList = solvingExamMapper.toDtos(solvingExamDAO.findByExamIdsAndUserId(userId, examsIds));
        return solvingExamsDTOList;
    }


    public QuestionToSolveDTO getQuestionByIncrementing(String examId, Integer incrementNumber) {
        int questionsCountInExam = examService.findQuestionsCountById(examId);


        SolvingExamsDTO solvingExam = getSolvingExamByExamIds(new HashSet<>(Arrays.asList(examId))).get(0);

        Integer questionNumberBeforeIncrement = solvingExam.getActualQuestionNumber();
        Integer actualQuestionNumberById = null;


        //TODO: what if number of questions in exam is greater than number in real questions
        if (questionNumberBeforeIncrement + incrementNumber >= 0
                && questionNumberBeforeIncrement + incrementNumber < questionsCountInExam
                && incrementNumber != 0) {
            solvingExamDAO.incrementActualQuestionById(solvingExam.getId(), incrementNumber);
            actualQuestionNumberById = solvingExamDAO.findActualQuestionNumberById(solvingExam.getId());
            while (actualQuestionNumberById == questionNumberBeforeIncrement) {
            } //it's mongo transaction hack
        }


        Integer questionNumberToGetFromDB = actualQuestionNumberById != null ? actualQuestionNumberById : questionNumberBeforeIncrement;
        String questionId = solvingExam.getQuestionPoolIdsWithAnswers().keySet().toArray()[questionNumberToGetFromDB].toString();

        return questionsService.findQuestionToSolveById(questionId);
    }

    public void saveAnswer(UserAnswerDTO userAnswerDTO) {
        String questionId = userAnswerDTO.getQuestionId();
        QuestionToSolveDTO question = questionsService.findQuestionToSolveById(questionId);
        UserAnswerForSolvingEntity userAnswerForSolvingEntity = new UserAnswerForSolvingEntity();
        if (question.getType().equals(QuestionType.CLOSE)) {
            userAnswerForSolvingEntity.setCloseAnswers(userAnswerDTO.getCloseAnswers());
        } else if (question.getType().equals(QuestionType.OPEN)) {
            userAnswerForSolvingEntity.setAnswerTextOrKey(userAnswerDTO.getTextAnswer());
        } else if (question.getType().equals(QuestionType.KEY)) {
            userAnswerForSolvingEntity.setAnswerTextOrKey(userAnswerDTO.getTextAnswer());
        }


        solvingExamDAO.updateAnswer(userAnswerDTO.getSolvingExamId(), questionId, userAnswerForSolvingEntity);

    }

    public Map<String, Boolean> findAllAnswerIdsBySolvingExamId(String solvingExamId) {
        Map<String, Boolean> map = new LinkedHashMap<>();
        Map<String, UserAnswerForSolvingEntity> userAnswerMap = solvingExamDAO.findAllAnswerIdsById(solvingExamId);
        userAnswerMap.keySet().forEach(p -> {
            //setting true if answer exists
            if (userAnswerMap.get(p) == null) {
                map.put(p, false);
            } else {
                map.put(p, true);
            }
        });
        return map;
    }

    public UserAnswerForSolvingEntity findAnswerBySolvingExamIdAndQuestionId(String solvingExamId, String questionId) {
        UserAnswerForSolvingEntity userAnswerForSolvingEntity = solvingExamDAO.findAnswerByIdAndQuestionId(solvingExamId, questionId);
        return userAnswerForSolvingEntity;
    }

    public void exitExam(String solvingExamId) {
        solvingExamDAO.updateEndedById(solvingExamId);
        gradeService.gradeExam(solvingExamId);
        scheduledFuture.cancel(true);

    }

    public String findOpenQuestionAnswerBySolvingExamIdAndQuestionId(String solvingExamId, String questionId) {
        return solvingExamDAO.findQuestioAnswerByIdAndQuestionId(solvingExamId, questionId);
    }

    public Date findStartExamTimeByExamIdAndUserId(String id, String userId) {
        return solvingExamDAO.findStartExamTimeByExamIdAndUserId(id,userId);
    }
}
