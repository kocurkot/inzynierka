package examsystem.services.mappers;

import examsystem.dtos.UserDTO;
import examsystem.entities.UserEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Component
public class UserMapper implements Mapper<UserEntity,UserDTO> {
    @Override
    public UserEntity getDocument(UserDTO userDTO) {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(userDTO.getEmail());
        userEntity.setLastName(userDTO.getLastName());
        userEntity.setFirstName(userDTO.getFirstName());
        userEntity.setId(userDTO.getId());
        userEntity.setAccessForStudentsPassword(userDTO.getAccessForStudentsPassword());
        userEntity.setRoles(userDTO.getRoles());
        userEntity.setMyTeacherAccessIds(userDTO.getMyTeacherAccessIds());
        userEntity.setAlbumNumber(userDTO.getAlbumNumber());
        return userEntity;
    }

    @Override
    public UserDTO getDto(UserEntity userEntity) {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(userEntity.getEmail());
        userDTO.setLastName(userEntity.getLastName());
        userDTO.setFirstName(userEntity.getFirstName());
        userDTO.setId(userEntity.getId());
        userDTO.setRoles(userEntity.getRoles());
        userDTO.setAccessForStudentsPassword(userEntity.getAccessForStudentsPassword());
        userDTO.setMyTeacherAccessIds(userEntity.getMyTeacherAccessIds());
        userDTO.setAlbumNumber(userEntity.getAlbumNumber());

        return userDTO;
    }
}
