package examsystem.services.mappers;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Wojciech on 07.10.2015.
 */
public interface Mapper<Document,Dto> {
    public Document getDocument(Dto dto);
    public Dto getDto(Document document);

    default List<Dto> toDtos(@NotNull List<Document> entities) {
        List<Dto> dtos = entities.stream().map(this::getDto).collect(Collectors.toList());
        return dtos;
    }

}
