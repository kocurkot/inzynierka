package examsystem.services.mappers;

import examsystem.dtos.SolvingExamsDTO;
import examsystem.entities.SolvingExamEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Wojciech on 08.11.2015.
 */
@Component
public class SolvingExamMapper implements Mapper<SolvingExamEntity,SolvingExamsDTO>{
    @Override
    public SolvingExamEntity getDocument(SolvingExamsDTO solvingExamsDTO) {
        SolvingExamEntity solvingExamEntity = new SolvingExamEntity();
        solvingExamEntity.setExamId(solvingExamsDTO.getExamId());
        solvingExamEntity.setId(solvingExamsDTO.getId());
        solvingExamEntity.setQuestionPoolIdsWithAnswers(solvingExamsDTO.getQuestionPoolIdsWithAnswers());
        solvingExamEntity.setActualQuestionNumber(solvingExamsDTO.getActualQuestionNumber());
        solvingExamEntity.setUserId(solvingExamsDTO.getUserId());
        solvingExamEntity.setEnded(solvingExamsDTO.getEnded());
        solvingExamEntity.setStartDate(solvingExamsDTO.getStartDate());
        return solvingExamEntity;
    }

    @Override
    public SolvingExamsDTO getDto(SolvingExamEntity solvingExamEntity) {
        SolvingExamsDTO solvingExamsDTO = new SolvingExamsDTO();
        solvingExamsDTO.setExamId(solvingExamEntity.getExamId());
        solvingExamsDTO.setId(solvingExamEntity.getId());
        solvingExamsDTO.setQuestionPoolIdsWithAnswers(solvingExamEntity.getQuestionPoolIdsWithAnswers());
        solvingExamsDTO.setActualQuestionNumber(solvingExamEntity.getActualQuestionNumber());
        solvingExamsDTO.setUserId(solvingExamEntity.getUserId());
        solvingExamsDTO.setEnded(solvingExamEntity.getEnded());
        solvingExamsDTO.setStartDate(solvingExamEntity.getStartDate());

        return solvingExamsDTO;
    }
}
