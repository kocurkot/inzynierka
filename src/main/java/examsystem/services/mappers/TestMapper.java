package examsystem.services.mappers;

import examsystem.dtos.TestDTO;
import examsystem.entities.TestEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Wojciech on 07.10.2015.
 */
@Component
public class TestMapper implements Mapper<TestEntity,TestDTO>{

    @Override
    public TestEntity getDocument(TestDTO testDTO) {
        TestEntity testEntity = new TestEntity();
        testEntity.setId(testDTO.getId());
        testEntity.setName(testDTO.getName());
        return testEntity;
    }

    @Override
    public TestDTO getDto(TestEntity testEntity) {
        TestDTO testDTO = new TestDTO();
        testDTO.setId(testEntity.getId());
        testDTO.setName(testEntity.getName());
        return testDTO;
    }
}
