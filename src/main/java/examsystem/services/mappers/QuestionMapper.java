package examsystem.services.mappers;

import examsystem.dtos.QuestionDTO;
import examsystem.entities.QuestionEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Wojciech on 08.10.2015.
 */
@Component
public class QuestionMapper implements Mapper<QuestionEntity,QuestionDTO> {
    @Override
    public QuestionEntity getDocument(QuestionDTO questionDTO) {
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setId(questionDTO.getId());
        questionEntity.setCloseAnswers(questionDTO.getCloseAnswers());
        questionEntity.setKeyAnswers(questionDTO.getKeyAnswers());
        questionEntity.setTestId(questionDTO.getTestId());
        questionEntity.setText(questionDTO.getText());
        questionEntity.setType(questionDTO.getType());
        questionEntity.setPoints(questionDTO.getPoints());
        return questionEntity;
    }

    @Override
    public QuestionDTO getDto(QuestionEntity questionEntity) {
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setId(questionEntity.getId());
        questionDTO.setCloseAnswers(questionEntity.getCloseAnswers());
        questionDTO.setKeyAnswers(questionEntity.getKeyAnswers());
        questionDTO.setTestId(questionEntity.getTestId());
        questionDTO.setText(questionEntity.getText());
        questionDTO.setType(questionEntity.getType());
        questionDTO.setPoints(questionEntity.getPoints());
        return questionDTO;
    }
}
