package examsystem.services.mappers;

import examsystem.dtos.ExamDTO;
import examsystem.entities.ExamEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Wojciech on 20.10.2015.
 */
@Component
public class ExamMapper implements Mapper<ExamEntity, ExamDTO> {
    @Override
    public ExamEntity getDocument(ExamDTO examDTO) {
        ExamEntity examEntity = new ExamEntity();
        examEntity.setId(examDTO.getId());
        examEntity.setStartDate(examDTO.getStartDate());
        examEntity.setEndDate(examDTO.getEndDate());
        examEntity.setPassword(examDTO.getPassword());
        examEntity.setTestId(examDTO.getTestId());
        examEntity.setDurationSeconds(examDTO.getDurationSeconds());
        examEntity.setQuestionsAmount(examDTO.getQuestionsAmount());
        examEntity.setRemove(examDTO.getRemove());
        examEntity.setName(examDTO.getName());
        examEntity.setQuestionGroupedCount(examDTO.getQuestionGroupedCount());
        return examEntity;
    }

    @Override
    public ExamDTO getDto(ExamEntity examEntity) {
        ExamDTO examDTO = new ExamDTO();
        examDTO.setId(examEntity.getId());
        examDTO.setStartDate(examEntity.getStartDate());
        examDTO.setEndDate(examEntity.getEndDate());
        examDTO.setPassword(examEntity.getPassword());
        examDTO.setTestId(examEntity.getTestId());
        examDTO.setDurationSeconds(examEntity.getDurationSeconds());
        examDTO.setQuestionsAmount(examEntity.getQuestionsAmount());
        examDTO.setRemove(examEntity.getRemove());
        examDTO.setName(examEntity.getName());
        examDTO.setQuestionGroupedCount(examEntity.getQuestionGroupedCount());


        return examDTO;
    }
}
