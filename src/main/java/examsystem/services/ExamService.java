package examsystem.services;

import examsystem.DAOs.ExamDAO;
import examsystem.dtos.*;
import examsystem.entities.ExamEntity;
import examsystem.entities.TestEntity;
import examsystem.services.mappers.ExamMapper;
import examsystem.services.mappers.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Wojciech on 22.10.2015.
 */
@Service
public class ExamService {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private ExamDAO examDAO;

    @Autowired
    private ExamMapper examMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private TestService testService;

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private SolvingExamService solvingExamService;

    public void saveNewExam(ExamDTO examDTO){
        ExamEntity examEntity = examMapper.getDocument(examDTO);
        examDAO.getCustom().save(examEntity);
    }

    public ExamEntity getExamById(String id) {
        return examDAO.getCustom().findOne(id);
    }

    public List<ExamTestDTO> getAvailableExams() {
        UserDTO loggedUser = userService.getLoggedUser();
        Set<String> myTeacherAccessIds = loggedUser.getMyTeacherAccessIds();
        List<ExamTestDTO> examTestDTOs = findExamsByTeacherIds(myTeacherAccessIds);
        Set<String> examsIds = examTestDTOs.stream()
                .map(ExamTestDTO::getExamDTO)
                .map(ExamDTO::getId)
                .collect(Collectors.toSet());

        //filtering started exams already

        List<SolvingExamsDTO> solvingExams = solvingExamService.getSolvingExamByExamIds(examsIds);
        Set<String> solvedExamIdsAlready = solvingExams
                .stream().map(SolvingExamsDTO::getExamId)
                .collect(Collectors.toSet());

        solvedExamIdsAlready.removeAll(getActualSolvingExamDtos(solvingExams));

        List<ExamTestDTO> objectsToRemove = examTestDTOs.stream()
                .filter(examTestDTO -> solvedExamIdsAlready.contains(examTestDTO.getExamDTO().getId()))
                .collect(Collectors.toList());
        examTestDTOs.removeAll(objectsToRemove);

        return examTestDTOs;
    }

    private Set<String> getActualSolvingExamDtos(List<SolvingExamsDTO> solvingExams) {
        return solvingExams.stream().filter(p -> p.getEnded() == null).map(SolvingExamsDTO::getExamId).collect(Collectors.toSet());
    }

    public List<ExamTestDTO> findExamsByTeacherIds(Set<String> myTeacherAccessIds) {
        List<TestDTO> tests = testService.findTestsByTeacherIds(myTeacherAccessIds);
        Set<String> testIds = new HashSet<>();
        tests.forEach(p->testIds.add(p.getId()));
        List<ExamEntity> examEntities = examDAO.findByTestIds(testIds);
        List<ExamTestDTO> examTestDTOs = new ArrayList<>();

        for(ExamEntity exam : examEntities){
            TestDTO testDto = tests.stream().filter(p->p.getId().equals(exam.getTestId())).findFirst().get();
            ExamTestDTO examTestDTO = new ExamTestDTO();
            examTestDTO.setExamDTO(examMapper.getDto(exam));
            examTestDTO.setTestDTO(testDto);
            examTestDTOs.add(examTestDTO);
        }

        return examTestDTOs;
    }

    public List<ExamTestDTO> findMyExams() {
        String userId = userService.getLoggedUser().getId();
        return findExamsByTeacherIds(new HashSet<>(Arrays.asList(userId)));
    }

    public Boolean removeExamById(String id) {
        String userId = userService.getLoggedUser().getId();

        ExamEntity examEntity = examDAO.getCustom().findOne(id);
        TestEntity testEntity = testService.findById(examEntity.getTestId());

        if(testEntity.getUserId().equals(userId)){
            examDAO.setAsRemoveByIdAndUserId(id);
            return true;
        }
        return false;
    }

    public Integer findQuestionsCountById(String examId){
        return findQuestionsCountByExamEntity(getExamById(examId));
    }

    public Integer findQuestionsCountByExamEntity(ExamEntity examEntity){
        int questionsCount = 0;
        for (String count : examEntity.getQuestionGroupedCount().values()) {
            int countInt = Integer.parseInt(count);
            questionsCount += countInt;
        }
        return questionsCount;
    }

    public Integer findQuestionsMaxPointsByExamEntity(ExamEntity examEntity) {
        int questionsMaxPoints = 0;
        for (String points : examEntity.getQuestionGroupedCount().keySet()) {
            int pointsInt = Integer.parseInt(points);
            int amount = Integer.parseInt(examEntity.getQuestionGroupedCount().get(points));
            questionsMaxPoints += pointsInt * amount;
        }
        return questionsMaxPoints;
    }
}
