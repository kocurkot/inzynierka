package examsystem.services;

import examsystem.DAOs.QuestionDAO;
import examsystem.dtos.Answer;
import examsystem.dtos.AnswerToSolve;
import examsystem.dtos.QuestionDTO;
import examsystem.dtos.QuestionToSolveDTO;
import examsystem.entities.QuestionEntity;
import examsystem.services.mappers.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Wojciech on 20.10.2015.
 */
@Service
public class QuestionsService {

    @Autowired
    QuestionDAO questionDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private QuestionMapper questionMapper;

    public Long countQuestionsForTestId(String testId) {
        Long questionsNumber = questionDAO.countQuestionsByTestId(testId);
        return questionsNumber;
    }

    public List<QuestionEntity> findQuestionIdsByTestId(String testId) {
        Query query = new Query(Criteria.where("testId").is(testId));
        query.fields().include("id");
        return mongoTemplate.find(query, QuestionEntity.class);
    }

    public List<QuestionEntity> findQuestionsByTestId(String testId) {
        Query query = new Query(Criteria.where("testId").is(testId));
        return mongoTemplate.find(query, QuestionEntity.class);
    }

    public QuestionToSolveDTO findQuestionToSolveById(String questionId) {
        QuestionEntity questionEntity = questionDAO.getCustom().findOne(questionId);
        QuestionToSolveDTO questionToSolveDTO = new QuestionToSolveDTO();
        questionToSolveDTO.setId(questionEntity.getId());

        if (questionEntity.getCloseAnswers() != null && !questionEntity.getCloseAnswers().isEmpty()) {

            List<AnswerToSolve> closeAnswersToSolve = new ArrayList<>();

            Long index = 1L;
            for (Answer answer : questionEntity.getCloseAnswers()) {
                AnswerToSolve answerToSolve = new AnswerToSolve();
                answerToSolve.setText(answer.getText());
                answerToSolve.setId(index);
                closeAnswersToSolve.add(answerToSolve);
                index++;
            }
            questionToSolveDTO.setCloseAnswers(closeAnswersToSolve);

        }
        questionToSolveDTO.setText(questionEntity.getText());
        questionToSolveDTO.setType(questionEntity.getType());

        return questionToSolveDTO;
    }

    public QuestionEntity findQuestionEntityById(String questionId){
        return questionDAO.getCustom().findOne(questionId);
    }

    public Map<String, Integer> findQuestionIdsAndPointsByTestId(String testId) {
        Query query = new Query(Criteria.where("testId").is(testId));
        query.fields().include("id");
        query.fields().include("points");
        List<QuestionEntity> questionEntities = mongoTemplate.find(query, QuestionEntity.class);
        Map<String, Integer> allQuestionIdsAndPoints = new HashMap<>();
        questionEntities.stream().forEach(p->{
            allQuestionIdsAndPoints.put(p.getId(),p.getPoints().intValue());
        });
        return allQuestionIdsAndPoints;
    }
}
