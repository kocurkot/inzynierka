package examsystem.services;

import examsystem.DAOs.UserDAO;
import examsystem.dtos.TeacherResourcesAccessDTO;
import examsystem.dtos.UserDTO;
import examsystem.entities.UserEntity;
import examsystem.exceptions.*;
import examsystem.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Wojciech on 11.10.2015.
 */
@Service
public class UserService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserMapper userMapper;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public UserEntity findUserByEmail(String email) throws UserNotFoundException{
        UserEntity userEntity = userDAO.getCustom().findByEmail(email);
        if(userEntity == null){
            throw new UserNotFoundException("User with email: " + email + " does not exist");
        }
        return userEntity;
    }

    public UserDTO getLoggedUser(){
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        return userMapper.getDto(userDAO.getCustom().findByEmail(username));
    }

    public String findStudentAccessPassword() {
        return userDAO.getCustom().findOne(getLoggedUser().getId()).getAccessForStudentsPassword();
    }

    public void saveStudentAccessPassword(String studentAccessPassword) throws BadUserRoleException {
        UserDTO loggedUser = getLoggedUser();
        if(!loggedUser.getRoles().contains("ROLE_TEACHER")){
            throw new BadUserRoleException("User does not have teacher role");
        }
        userDAO.saveStudentAccessPassword(studentAccessPassword, loggedUser.getId());
    }

    public void saveTeacherResourcesAccessForStudent(TeacherResourcesAccessDTO teacherResourcesAccessDTO) throws BadTeacherResourcesAccessData {
        UserEntity teacherUserEntity = userDAO.getCustom().findByEmail(teacherResourcesAccessDTO.getEmail());
        if(teacherUserEntity == null || !isTeacherResourcesPasswordValid(teacherResourcesAccessDTO, teacherUserEntity)){
            logger.info("bad teacher credentials: " + teacherResourcesAccessDTO.getEmail());
            throw new BadTeacherResourcesAccessData();
        }

        logger.info("proper teacher resources credentials: " + teacherResourcesAccessDTO.getEmail());

        UserDTO loggedStudentUser = getLoggedUser();
        userDAO.updateMyTeachersAccess(loggedStudentUser.getId(), teacherUserEntity.getId());

    }

    protected Boolean isTeacherResourcesPasswordValid(TeacherResourcesAccessDTO teacherResourcesAccessDTO, UserEntity teacherUserEntity) {
        UserEntity userEntity = userDAO.getCustom().findByEmail(teacherResourcesAccessDTO.getEmail());
        if(userEntity.getAccessForStudentsPassword().equals(teacherResourcesAccessDTO.getPassword())){
            return true;
        }
        return false;
    }

    public List<UserDTO> findMyTeachers() {
        UserDTO loggedUser = getLoggedUser();
        Set<String> myTeacherAccessIds = loggedUser.getMyTeacherAccessIds() != null
                ? loggedUser.getMyTeacherAccessIds() : new HashSet<>();
        return userMapper.toDtos(userDAO.findEmailAndNameByUserIds(myTeacherAccessIds));
    }

    public Set<String> findMyTeachersIds(){
        UserDTO loggedUser = getLoggedUser();
        return loggedUser.getMyTeacherAccessIds();
    }

    public UserEntity findUserById(String studentUserId) {
        return userDAO.getCustom().findOne(studentUserId);
    }

    public void saveNewStudent(UserDTO userDTO) throws UserMailExistsException, UserAlbumNumberExistsException {
        if(mailExistsInDb(userDTO.getEmail())){
            throw new UserMailExistsException();
        }
        if(albumNumberExistsInDb(userDTO.getAlbumNumber())){
            throw new UserAlbumNumberExistsException();
        }


        userDTO.setRoles(Arrays.asList("ROLE_STUDENT"));
        UserEntity userEntity = userMapper.getDocument(userDTO);
        userEntity.setPassword(userDTO.getFirstName()+userDTO.getLastName());
        userDAO.getCustom().save(userEntity);
    }

    private Boolean albumNumberExistsInDb(Number albumNumber) {
        UserEntity userEntity = userDAO.getCustom().findByAlbumNumber(albumNumber);

        if(userEntity == null){
            return false;
        }else{
            return true;
        }
    }

    private Boolean mailExistsInDb(String email) {
        UserEntity userEntity = userDAO.getCustom().findByEmail(email);
        if(userEntity == null){
            return false;
        }else{
            return true;
        }
    }

    public Boolean isPasswordGood(String oldPassword) {
        UserEntity userEntity = findUserById(getLoggedUser().getId());
        if(userEntity.getPassword().equals(oldPassword)){
            return true;
        }
        return false;
    }

    public void saveNewPassword(String newPassword1) {
        UserDTO loggedUser = getLoggedUser();
        userDAO.saveNewPasswordByUserId(newPassword1, loggedUser.getId());
    }
}
