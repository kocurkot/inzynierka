package examsystem;

import examsystem.DAOs.CustomUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

/**
 * Created by Wojciech on 19.09.2015.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()

                .antMatchers("/js/services/teacher/**").hasRole("TEACHER")
                .antMatchers("/js/teacher/**").hasRole("TEACHER")
                .antMatchers("/js/services/student/**").hasRole("STUDENT")
                .antMatchers("/js/student/**").hasRole("STUDENT")
                .antMatchers("/js/changepasswordController.js").hasAnyRole("STUDENT", "TEACHER")

                .antMatchers("/testManager/**").hasRole("TEACHER")
                .antMatchers("/user/teacher/**").hasRole("TEACHER")
                .antMatchers("/user/student/**").hasRole("STUDENT")
                .antMatchers("/user/solveExam/**").hasRole("STUDENT")
                .antMatchers("/teacher/myteachers").hasRole("STUDENT")
                .anyRequest().authenticated()

                .and()

                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
        ;
    }

    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/css/**")
                .antMatchers("/js/lib/**")
                .antMatchers("/js/login.js")
                .antMatchers("/bowercomponents/**");
    }

    @Autowired
    MongoUserDetailsService userDetailsService;

    @Autowired
    CustomUserDAO customUserDAO;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

//        DataFactory df = new DataFactory();
//        for(int i = 0; i < 100; i++){
//            UserEntity userEntity = new UserEntity();
//            userEntity.setEmail(df.getEmailAddress());
//            userEntity.setFirstName(df.getFirstName());
//            userEntity.setLastName(df.getLastName());
//            userEntity.setPassword("secret");
//            userEntity.setRoles(Arrays.asList("STUDENT"));
//            customUserDAO.save(userEntity);
//        }
        auth
                .userDetailsService(userDetailsService)
        ;
    }

    @Bean
    public SpringSecurityDialect additionalDialects(){
        return new SpringSecurityDialect();
    }

}
