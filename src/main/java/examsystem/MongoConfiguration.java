package examsystem;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wojtek on 2015-08-19.
 */

@Configuration
@ComponentScan
@EnableMongoRepositories
public class MongoConfiguration {

    public @Bean
    MongoDbFactory mongoDbFactory() throws Exception {
        ServerAddress serverAddress = new ServerAddress("ds039504.mongolab.com",39504);


        UserCredentials userCredentials = new UserCredentials("admin","QW12qw12");

        MongoClient mongoClient = new MongoClient(Collections.singletonList(serverAddress),
                Collections.singletonList(MongoCredential.createScramSha1Credential("admin","examsystem","QW12qw12".toCharArray())));

      //  return new SimpleMongoDbFactory(mongoClient, "examsystem");
       return new SimpleMongoDbFactory(new MongoClient(), "examSystem");
    }

    public @Bean
    MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
        return mongoTemplate;
    }
}
